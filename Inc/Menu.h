#ifndef __MENU_H
#define __MENU_H

#include "RF_modules.h"
#include "st25_discovery_ts.h"
#include "st25_discovery_lcd.h"

void InitScreen(void);
//void PrintSelectionMenu(char *firstElement, char *secondElement, char *thirdElement, char *fourthElement, char *fivethElement, char *sixthElement, char *seventhElement);
//void PrintParametersMenu(RFModule *module);
//void PrintStatistic(Statistics* stat);
uint16_t TouchProcessing(uint16_t index, TS_StateTypeDef *TsState);
void UpdateScreen(uint16_t indexMenu, RFModule *module);
#endif
