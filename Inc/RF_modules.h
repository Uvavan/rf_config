#ifndef __RF_MODULES_H
#define __RF_MODULES_H

#include "TypeDef.h"

#define MASTER 0
#define SLAVE 1

typedef struct {
   char *nameModule;
   uint8_t (*RFModuleInit)(void*);
   uint8_t (*RFModuleConfig)(void*);
   float bandwidth;
   uint32_t centreFr;
   uint32_t timePing;
   uint8_t spreadingFactor;
   uint8_t codingRate;
   uint8_t payloadLength;
   uint8_t progPreamble;
   bool EnCRC;
   uint8_t transmitPower;
   uint8_t OperationMode; // 0 - master, 1 - slave
} RFModule;

extern RFModule modules[];

void ReadPConfigParamWithRF(RFModule* module);

#endif
