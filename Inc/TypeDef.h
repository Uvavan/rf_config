#ifndef __TYPE_DEF_H
#define __TYPE_DEF_H

#include <stdint.h>
#include <stdbool.h>

typedef struct {
   uint8_t *nameModule;
   int32_t allSend;
   int32_t notConfirm;
   int32_t durationSend;
   int32_t errorCRC;
   uint16_t distance;
   int8_t RSSI;
   uint8_t precentSuccessful;
   bool newDataIsReady;
} Statistics;

#endif

/*
Spreading Factor  - 6 - 12
Bandwidth - {500, 250, 125, 62.5, 41.7, 31.25, 20.8, 15.6, 10.4, 7,8} kHz
Coding Rate = 1-4
Payload Length - 1 - 100
Programmed Preamble - 1-100
Enable CRC - false/true
Centre Frequency - 100000 - 3000000  kHz
Transmit Power - 0 - 100 dBm
TimePing - int32_t*/

/*�����:
AllSend - int32_t 
NotConfirm - int32_t
PrecentSuccessful - uint8_t
RSSI - int8_t
DurationSend - int32_t 
ErrorCrc - int32_t
Distance - uint16_t */
