#include "RF_modules.h"

uint8_t InitRFM95W(void* data);
uint8_t ConfigRFM95W(void* data);

uint8_t InitRFM98W(void* data);
uint8_t ConfigRFM98W(void* data);

uint8_t InitRFM95PW(void* data);
uint8_t ConfigRFM95PW(void* data);

uint8_t InitSX1276MB(void* data);
uint8_t ConfigSX1276MB(void* data);

uint8_t InitSX1276RF(void* data);
uint8_t ConfigSX1276RF(void* data);

uint8_t InitE282G4M12S(void* data);
uint8_t ConfigE282G4M12S(void* data);

uint8_t InitE282G4M20S(void* data);
uint8_t ConfigE282G4M20S(void* data);

RFModule modules[] = {
  { 
   "RFM95W",
   InitRFM95W, // RFinit
   ConfigRFM95W, // RFConfig
   500,
   3000000,
   23565,
   12,
   4,
   100,
   100,
   false,
   100    
  }, 
  { 
   "RFM98W",
   InitRFM98W, // RFinit
   ConfigRFM98W, // RFConfig
   500,
   3000000,
   23565,
   12,
   4,
   100,
   100,
   false,
   100 
  }, 
  { 
   "RFM95PW",
   InitRFM95PW, // RFinit
   ConfigRFM95PW, // RFConfig
   500,
   3000000,
   23565,
   12,
   4,
   100,
   100,
   false,
   100 
  }, 
  { 
   "SX1276MB",
   InitSX1276MB, // RFinit
   ConfigSX1276MB, // RFConfig
   500,
   3000000,
   23565,
   12,
   4,
   100,
   100,
   false,
   100 
  }, 
  { 
   "SX1276RF",
   InitSX1276RF, // RFinit
   ConfigSX1276RF, // RFConfig
   500,
   3000000,
   23565,
   12,
   4,
   100,
   100,
   false,
   100 
  }, 
  { 
   "E28-2G4M12S",
   InitE282G4M12S, // RFinit
   ConfigE282G4M12S, // RFConfig
   500,
   3000000,
   23565,
   12,
   4,
   100,
   100,
   false,
   100 
  }, 
  { 
   "E28-2G4M20S",
   InitE282G4M20S, // RFinit
   ConfigE282G4M20S, // RFConfig
   500,
   3000000,
   23565,
   12,
   4,
   100,
   100,
   false,
   100 
  }, 
};

uint8_t InitRFM95W(void* data) {
   RFModule *module = (RFModule*) data;
   // init rf module code
   return 0;
}

uint8_t ConfigRFM95W(void* data) {
   RFModule *module = (RFModule*) data;
   // config rf module code
   return 0;
} 

uint8_t InitRFM98W(void* data) {
   RFModule *module = (RFModule*) data;
   // init rf module code
   return 0;
}

uint8_t ConfigRFM98W(void* data) {
   RFModule *module = (RFModule*) data;
   // config rf module code
   return 0;
} 

uint8_t InitRFM95PW(void* data) {
   RFModule *module = (RFModule*) data;
   // init rf module code
   return 0;
}

uint8_t ConfigRFM95PW(void* data) {
   RFModule *module = (RFModule*) data;
   // config rf module code
   return 0;
} 

uint8_t InitSX1276MB(void* data) {
   RFModule *module = (RFModule*) data;
   // init rf module code
   return 0;
}

uint8_t ConfigSX1276MB(void* data) {
   RFModule *module = (RFModule*) data;
   // config rf module code
   return 0;
} 

uint8_t InitSX1276RF(void* data) {
   RFModule *module = (RFModule*) data;
   // init rf module code
   return 0;
}

uint8_t ConfigSX1276RF(void* data) {
   RFModule *module = (RFModule*) data;
   // config rf module code
   return 0;
} 

uint8_t InitE282G4M12S(void* data) {
   RFModule *module = (RFModule*) data;
   // init rf module code
   return 0;
}

uint8_t ConfigE282G4M12S(void* data) {
   RFModule *module = (RFModule*) data;
   // config rf module code
   return 0;
} 

uint8_t InitE282G4M20S(void* data) {
   RFModule *module = (RFModule*) data;
   // init rf module code
   return 0;
}

uint8_t ConfigE282G4M20S(void* data) {
   RFModule *module = (RFModule*) data;
   // config rf module code
   return 0;
} 

void ReadPConfigParamWithRF(RFModule* module) {
   // read param code
}
