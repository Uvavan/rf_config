/**
  ******************************************************************************
  * @file    ili9341.c
  * @author  MCD Application Team
  * @version V1.0.2
  * @date    02-December-2014
  * @brief   This file includes the LCD driver for ILI9341 LCD.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT(c) 2014 STMicroelectronics</center></h2>
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */ 

/* Includes ------------------------------------------------------------------*/
#include "ili9341_cube.h"

/** @addtogroup BSP
  * @{
  */ 

/** @addtogroup Components
  * @{
  */ 
  
/** @addtogroup ILI9341
  * @brief This file provides a set of functions needed to drive the 
  *        ILI9341 LCD.
  * @{
  */

/** @defgroup ILI9341_Private_TypesDefinitions
  * @{
  */ 
/**
  * @}
  */ 

/** @defgroup ILI9341_Private_Defines
  * @{
  */
/**
  * @}
  */ 
  
/** @defgroup ILI9341_Private_Macros
  * @{
  */
/**
  * @}
  */  

/** @defgroup ILI9341_Private_Variables
  * @{
  */ 

LCD_DrvTypeDef   ili9341_drv = 
{
  ili9341_Init,
  ili9341_ReadID,
  ili9341_DisplayOn,
  ili9341_DisplayOff,
  ili9341_SetCursor,
  ili9341_WritePixel,
  ili9341_ReadPixel,
  ili9341_SetDisplayWindow,
  ili9341_DrawHLine,
  ili9341_DrawVLine,
  ili9341_GetLcdPixelWidth,
  ili9341_GetLcdPixelHeight,
  0,
  0,    
};

/**
  * @}
  */ 
  
/** @defgroup ILI9341_Private_FunctionPrototypes
  * @{
  */

/**
  * @}
  */ 
  
/** @defgroup ILI9341_Private_Functions
  * @{
  */   

/**
  * @brief  Power on the LCD.
  * @param  None
  * @retval None
  */
#define ILI9341_INIT_DELAYS 30
void ili9341_Init(void)
{
  /* Initialize ILI9341 low level bus layer ----------------------------------*/
  LCD_IO_Init();
  
  /* Configure LCD */
	LCD_Delay(ILI9341_INIT_DELAYS);
  ili9341_WriteReg(0xCA);
  ili9341_WriteData(0xC3);
 	LCD_Delay(ILI9341_INIT_DELAYS);
	/************* Start Initial Sequence **********/
	///ILI9341_TM2.4  信创天源 9254 2012-06-07//
	ili9341_WriteReg(0x11);  //Exit Sleep
	LCD_Delay(ILI9341_INIT_DELAYS);
	ili9341_WriteReg(0xcf); // Power control B
	ili9341_WriteData(0x00);//	
	ili9341_WriteData(0xaa);//	
	ili9341_WriteData(0xb0);//
	LCD_Delay(ILI9341_INIT_DELAYS);
	ili9341_WriteReg(0xEF); // Command not listed in LCD datasheet 1.10.20110415	
	ili9341_WriteData(0x03);//	
	ili9341_WriteData(0x80);//	
	ili9341_WriteData(0x02);//
		LCD_Delay(ILI9341_INIT_DELAYS);
	ili9341_WriteReg(0xed); // Power On sequence control
	ili9341_WriteData(0x67);//	
	ili9341_WriteData(0x03);//	
	ili9341_WriteData(0x12);//	
	ili9341_WriteData(0x81);//
		LCD_Delay(ILI9341_INIT_DELAYS);
	ili9341_WriteReg(0xe8); // Driver Timing control A (E8h)
	// ili9341_WriteData(0x83);//
	// ili9341_WriteData(0x11);//
	// ili9341_WriteData(0x78);//
  ili9341_WriteData(0x84);//	
  ili9341_WriteData(0x11);//	
	ili9341_WriteData(0x7A);//
	LCD_Delay(ILI9341_INIT_DELAYS);
	ili9341_WriteReg(0xCB);// Power Control A
	ili9341_WriteData(0x39);//AP[2:0]	
	ili9341_WriteData(0x2c);//	
	ili9341_WriteData(0x00);//	
	ili9341_WriteData(0x34);//	
	ili9341_WriteData(0x02);//
		LCD_Delay(ILI9341_INIT_DELAYS);
	ili9341_WriteReg(0xea); // Driver timing control B
	// ili9341_WriteData(0x00);//
	// ili9341_WriteData(0x00);
	ili9341_WriteData(0x66);//
	ili9341_WriteData(0x00);
	LCD_Delay(ILI9341_INIT_DELAYS);
	ili9341_WriteReg(0xf7); // Pump ratio control
	ili9341_WriteData(0x20);//
	LCD_Delay(ILI9341_INIT_DELAYS);
	ili9341_WriteReg(0xCA); // not listed in LCD datasheet
	ili9341_WriteData(0x83); 	
	ili9341_WriteData(0x02);	
	ili9341_WriteData(0x00);
	LCD_Delay(ILI9341_INIT_DELAYS);
	ili9341_WriteReg(0xea); // Driver timing control B
	ili9341_WriteData(0x00);//
	ili9341_WriteData(0x00);//
		LCD_Delay(ILI9341_INIT_DELAYS);
	ili9341_WriteReg(0xC0); //Set GVDD 
	ili9341_WriteData(0x18); //VRH[5:0]	21
	LCD_Delay(ILI9341_INIT_DELAYS);
	ili9341_WriteReg(0xC1); //Set power step-up
	ili9341_WriteData(0x11); //SAP[2:0];BT[3:0]   04                             
	
	/******颜色浅***c7 0xc4****/
	//delay(120);
	//ili9341_WriteReg(0xC5); //set vcom	
	//ili9341_WriteData(0x3E);  //3E	
	//ili9341_WriteData(0x25);  //25 

	/********************/
	/******颜色浅些*c7 0xc7*****/
	//ili9341_WriteReg(0xC5); //set vcom
	//ili9341_WriteData(0x3e);  //0x3e
	//ili9341_WriteData(0x20);  //0x20
	/********************/
	/******颜色黑***c7 0xca*****/
	ili9341_WriteReg(0xC5); //set vcom
	ili9341_WriteData(0x3e);  //3e	
	ili9341_WriteData(0x15);  //15    
	/********************/
	
	// Interface Control 
	// ili9341_WriteReg(0xF6);
	// ili9341_WriteData(0x01);  //
	// ili9341_WriteData(0x03);  // 
	// ili9341_WriteData(0x20);  // 

  LCD_Delay(ILI9341_INIT_DELAYS);
	ili9341_WriteReg(0x36);
	
  ili9341_WriteData(0x28); 		 // Memory Access Control //48
	//ili9341_WriteData(0x00);
		LCD_Delay(ILI9341_INIT_DELAYS);
	//ili9341_WriteReg(0x37);     // Vertical Scrolling Start Address
	//ili9341_WriteData(0x00);
	//ili9341_WriteData(0x00);
	
	// Frame Rate Control
	
	ili9341_WriteReg(0xB1);  
	ili9341_WriteData(0x00); //
	ili9341_WriteData(0x1b);    
	ili9341_WriteData(0x10); //   
	
	// DIsplay Inversion Control
	// ili9341_WriteReg(0xB4);
	// ili9341_WriteData(0x00);

	ili9341_WriteReg(0x34); // Tearing Effect Line Off
	LCD_Delay(ILI9341_INIT_DELAYS);
	// Display Function Control
	ili9341_WriteReg(0xB6);
	ili9341_WriteData(0x0A); //Display Function Control  0x000a   
	ili9341_WriteData(0x82);
	ili9341_WriteData(0x27);
	
	// VCOM Control
	
	// ili9341_WriteReg(0xC7); //Set VCOMH/VCOML VMF[6:0] 
	// ili9341_WriteData(0xC4);  //C4 c7 ca
	
	// RGB Interface Signal Control
	// ili9341_WriteReg(0xB0);
	// ili9341_WriteData(0x00);
	// ili9341_WriteData(0x00);
	// Color Set - LUT Management
	
	//Gamma set
	ili9341_WriteReg(0x26); 
	ili9341_WriteData(0x01);
	ili9341_WriteReg(0xF2);
	ili9341_WriteData(0x00); //3Gamma Function Disable     
	/************4*****hao****************/
	

	ili9341_WriteReg(0xE0); //Set Gamma
	ili9341_WriteData(0x0F);
	ili9341_WriteData(0x3a);
	ili9341_WriteData(0x36);
	ili9341_WriteData(0x0b);
	ili9341_WriteData(0x0d);
	ili9341_WriteData(0x06);
	ili9341_WriteData(0x4c);
	ili9341_WriteData(0x91);
	ili9341_WriteData(0x31);
	ili9341_WriteData(0x08);
	ili9341_WriteData(0x10);
	ili9341_WriteData(0x04);
	ili9341_WriteData(0x11);
	ili9341_WriteData(0x0c);
	ili9341_WriteData(0x00);
	ili9341_WriteReg(0xE1); //Set Gamma
	ili9341_WriteData(0x00);
	ili9341_WriteData(0x06);
	ili9341_WriteData(0x0a);
	ili9341_WriteData(0x05);
	ili9341_WriteData(0x12);
	ili9341_WriteData(0x09);
	ili9341_WriteData(0x2c);
	ili9341_WriteData(0x92);
	ili9341_WriteData(0x3f);
	ili9341_WriteData(0x08);
	ili9341_WriteData(0x0e);
	ili9341_WriteData(0x0b);
	ili9341_WriteData(0x2e);
	ili9341_WriteData(0x33);
	ili9341_WriteData(0x0F);
   

	// Pixel Format
	
	ili9341_WriteReg(0x3A); 
	ili9341_WriteData(0x55);
	LCD_Delay(ILI9341_INIT_DELAYS);

	// Display Normal Mode
	// ili9341_WriteReg(0x13);
	
	// Display Inversion On - revert colors
	// ili9341_WriteReg(0x21);
	
	// ili9341_WriteReg(LCD_SLEEP_OUT);
	// delay(200);
	
	// ili9341_WriteReg(0x38); // Idle Mode Off
	// delay(200);
	
	// ili9341_WriteReg(LCD_DISPLAY_ON);
	// ili9341_WriteReg(LCD_GRAM);
	
  //ili9341_WriteReg(0x29); //display on 	
	/*
	ili9341_WriteReg(0x2A);	
	ili9341_WriteData(0x00);	
	ili9341_WriteData(0x00);	
	ili9341_WriteData(0x00);	
  ili9341_WriteData(0xEF);
	ili9341_WriteReg(0x2B);
	ili9341_WriteData(0x00);
	ili9341_WriteData(0x00);
  ili9341_WriteData(0x01);
	ili9341_WriteData(0x3F);
	*/
	ili9341_WriteReg(0x2A);	
	ili9341_WriteData(0x00);	
	ili9341_WriteData(0x00);
	ili9341_WriteData(((ILI9341_LCD_PIXEL_WIDTH-1)&0xFF00)>>8);	
	ili9341_WriteData((ILI9341_LCD_PIXEL_WIDTH-1)&0x00FF);

	ili9341_WriteReg(0x2B);
	ili9341_WriteData(0x00);
	ili9341_WriteData(0x00);
	ili9341_WriteData(((ILI9341_LCD_PIXEL_HEIGHT-1)&0xFF00)>>8);	
  ili9341_WriteData((ILI9341_LCD_PIXEL_HEIGHT-1)&0x00FF);
/*
	 ili9341_WriteReg(0x2C); //GRAM start writing

	for (int i=0; i<ILI9341_LCD_PIXEL_WIDTH*ILI9341_LCD_PIXEL_HEIGHT;i++)
	{
		ili9341_WritePixel(ILI9341_NO_CURSOR,ILI9341_NO_CURSOR,0x3D5B);
  }
  */
	ili9341_WriteReg(0x29); //display on
	
}

/**
  * @brief  Disables the Display.
  * @param  None
  * @retval LCD Register Value.
  */
uint16_t ili9341_ReadID(void)
{
  LCD_IO_Init();
  return ((uint16_t)ili9341_ReadData(LCD_READ_ID4, LCD_READ_ID4_SIZE));
}

/**
  * @brief  Enables the Display.
  * @param  None
  * @retval None
  */
void ili9341_DisplayOn(void)
{
  /* Display On */
  ili9341_WriteReg(LCD_DISPLAY_ON);
}


/**
  * @brief  Disables the Display.
  * @param  None
  * @retval None
  */
void ili9341_DisplayOff(void)
{
  /* Display Off */
  ili9341_WriteReg(LCD_DISPLAY_OFF);
}


void ili9341_SetCursor(uint16_t Xpos, uint16_t Ypos)
{
	if(((Xpos < ILI9341_LCD_PIXEL_WIDTH) && (Ypos < ILI9341_LCD_PIXEL_HEIGHT)) && 
		((Xpos != ILI9341_NO_CURSOR) && (Ypos != ILI9341_NO_CURSOR)))
	{
		ili9341_WriteReg(LCD_COLUMN_ADDR);
		ili9341_WriteData((Xpos&0xFF00)>>8);
		ili9341_WriteData(Xpos&0x00FF);
		ili9341_WriteData((ILI9341_LCD_PIXEL_WIDTH&0xFF00)>>8);
		ili9341_WriteData(Xpos&0x00FF);

		ili9341_WriteReg(LCD_PAGE_ADDR);
		ili9341_WriteData((Ypos&0xFF00)>>8);
		ili9341_WriteData(Ypos&0x00FF);
		ili9341_WriteData((ILI9341_LCD_PIXEL_HEIGHT&0xFF00)>>8);
		ili9341_WriteData(Ypos&0x00FF);

	//	ili9341_WriteReg(LCD_DISPLAY_ON); //display on
	//	ili9341_WriteReg(LCD_GRAM); //GRAM start writing
	}
	
}


void ili9341_SetDisplayWindow(uint16_t Xpos, uint16_t Ypos, uint16_t Width, uint16_t Height)
{
	uint16_t Xend = Xpos+Width;
	uint16_t Yend = Ypos+Height;

	if(Xend > ILI9341_LCD_PIXEL_WIDTH) 
		Xend = ILI9341_LCD_PIXEL_WIDTH;
	if(Yend > ILI9341_LCD_PIXEL_HEIGHT)
		Yend = ILI9341_LCD_PIXEL_HEIGHT;
	
	if(Xpos > ILI9341_LCD_PIXEL_WIDTH) 
		Xpos = ILI9341_LCD_PIXEL_WIDTH;
	if(Ypos > ILI9341_LCD_PIXEL_HEIGHT)
		Ypos = ILI9341_LCD_PIXEL_HEIGHT;

		ili9341_WriteReg(LCD_COLUMN_ADDR);
		ili9341_WriteData((Xpos&0xFF00)>>8);
		ili9341_WriteData(Xpos&0x00FF);
		ili9341_WriteData((Xend&0xFF00)>>8);
		ili9341_WriteData(Xend&0x00FF);
		
		ili9341_WriteReg(LCD_PAGE_ADDR);
		ili9341_WriteData((Ypos&0xFF00)>>8);
		ili9341_WriteData(Ypos&0x00FF);
		ili9341_WriteData((Yend&0xFF00)>>8);
		ili9341_WriteData(Yend&0x00FF);
	

}


static uint8_t ili9341_ram_mode = 0;
void ili9341_WritePixel(uint16_t Xpos, uint16_t Ypos, uint16_t Val)
{
	// If ILI9341_KEEP_CURSOR is used for Xpos or Ypos -> SetCursor does nothing 
	// This allows to write 9341 RAM using HW pixel increment
	ili9341_SetCursor(Xpos, Ypos);
	if(!ili9341_ram_mode) ili9341_WriteReg(LCD_GRAM);
	LCD_IO_WriteData16(Val);
}	

static uint8_t ili9341_readmem_mode = 0;
uint16_t ili9341_ReadPixel(uint16_t Xpos, uint16_t Ypos)
{
  uint32_t raw_pix;
	// If ILI9341_KEEP_CURSOR is used for Xpos or Ypos -> SetCursor does nothing 
	// This allows to read 9341 RAM using HW pixel increment
  ili9341_SetDisplayWindow(Xpos, Ypos, ILI9341_LCD_PIXEL_WIDTH,ILI9341_LCD_PIXEL_HEIGHT);
	if(!ili9341_readmem_mode)
  {
    /* Read 4 bytes: 1 dummy byte + Red ,Green & Blue bytes */
    raw_pix = ili9341_ReadData(LCD_RAMRD,4);
  } else {
    /* Don't go there on purpose due to the above SetDisplayWindow
       The READ_MEM_CONTINUE command miss some data when used with SPI SOFT NSS
    */
    raw_pix = ili9341_ReadData(LCD_READ_MEM_CONTINUE,4);
  }

  /* ILI returns the data as: dddddddd rrrrrddd ggggggdd bbbbbddd
  d: dummy bit, r: red bit, g: green bit, b: blue bit */
  uint8_t red = (raw_pix & 0xf80000) >> 19;
  uint8_t green = (raw_pix & 0xfc00) >> 10;
  uint8_t blue = (raw_pix & 0xf8) >> 3;
  
  /* Reassenble data on 16bits: rrrrrggg gggbbbbb */
  return  (red << 11) |
          (green << 5) |
          (blue);
  
}	


/**
  * @brief  Draw vertical line.
  * @param  RGB_Code: Specifies the RGB color   
  * @param  Xpos:     specifies the X position.
  * @param  Ypos:     specifies the Y position.
  * @param  Length:   specifies the Line length.  
  * @retval None
  */
void ili9341_DrawHLine(uint16_t RGB_Code, uint16_t Xpos, uint16_t Ypos, uint16_t Length)
{
  uint16_t i = 0;
  
  /* Set Cursor */
  ili9341_SetDisplayWindow(Xpos, Ypos, Length, 1); 
  
  for(i = 0; i < Length; i++)
  {
    /* Write 16-bit GRAM Reg */
    ili9341_WritePixel(ILI9341_NO_CURSOR,ILI9341_NO_CURSOR,RGB_Code);
  }  
}

/**
  * @brief  Draw vertical line.
  * @param  RGB_Code: Specifies the RGB color    
  * @param  Xpos:     specifies the X position.
  * @param  Ypos:     specifies the Y position.
  * @param  Length:   specifies the Line length.  
  * @retval None
  */
void ili9341_DrawVLine(uint16_t RGB_Code, uint16_t Xpos, uint16_t Ypos, uint16_t Length)
{
  uint16_t i = 0;

  
  /* Set Cursor */
  ili9341_SetDisplayWindow(Xpos, Ypos, 0, Length); 

  for(i = 0; i < Length; i++)
  {
    /* Write 16-bit GRAM Reg */
    ili9341_WritePixel(ILI9341_NO_CURSOR,ILI9341_NO_CURSOR,RGB_Code);
  }
  
}


/**
  * @brief  Writes  to the selected LCD register.
  * @param  LCD_Reg: address of the selected register.
  * @retval None
  */
void ili9341_WriteReg(uint8_t LCD_Reg)
{
  LCD_IO_WriteReg(LCD_Reg);
	ili9341_ram_mode = (LCD_Reg == LCD_GRAM);
	ili9341_readmem_mode = (LCD_Reg == LCD_RAMRD) || (LCD_Reg == LCD_READ_MEM_CONTINUE);
}

/**
  * @brief  Writes data to the selected LCD register.
  * @param  LCD_Reg: address of the selected register.
  * @retval None
  */
void ili9341_WriteData(uint16_t RegValue)
{
  LCD_IO_WriteData(RegValue);
}

/**
  * @brief  Reads the selected LCD Register.
  * @param  RegValue: Address of the register to read
  * @param  ReadSize: Number of bytes to read
  * @retval LCD Register Value.
  */
uint32_t ili9341_ReadData(uint16_t RegValue, uint8_t ReadSize)
{
  /* Read a max of 4 bytes */
 	ili9341_readmem_mode = (RegValue == LCD_RAMRD) || (RegValue == LCD_READ_MEM_CONTINUE);
 return (LCD_IO_ReadData(RegValue, ReadSize));
}

/**
  * @brief  Get LCD PIXEL WIDTH.
  * @param  None
  * @retval LCD PIXEL WIDTH.
  */
uint16_t ili9341_GetLcdPixelWidth(void)
{
  /* Return LCD PIXEL WIDTH */
  return ILI9341_LCD_PIXEL_WIDTH;
}

/**
  * @brief  Get LCD PIXEL HEIGHT.
  * @param  None
  * @retval LCD PIXEL HEIGHT.
  */
uint16_t ili9341_GetLcdPixelHeight(void)
{
  /* Return LCD PIXEL HEIGHT */
  return ILI9341_LCD_PIXEL_HEIGHT;
}

/**
  * @}
  */ 

/**
  * @}
  */ 
  
/**
  * @}
  */ 

/**
  * @}
  */
  
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
