/**
  ******************************************************************************
  * @file    st25_discovery.c 
  * @author  MMY Application Team
  * @version $Revision$
  * @date    $Date$
  * @brief   This file provides set of firmware functions to manage Leds and
  *          push-button available on ST25-Discovery Kit from STMicroelectronics.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2017 STMicroelectronics</center></h2>
  *
  * Licensed under ST MYLIBERTY SOFTWARE LICENSE AGREEMENT (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/myliberty  
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied,
  * AND SPECIFICALLY DISCLAIMING THE IMPLIED WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */ 
  
/* Includes ------------------------------------------------------------------*/
#include "st25_discovery.h"
#if defined(USE_NFCTAG)
  #include "st25_discovery_nfctag.h"
#endif

/** @defgroup BSP BSP
  * @{
  */ 

/** @addtogroup ST25_DISCOVERY_BSP ST25 DISCOVERY
  * @brief    The Boa?rd support package for the ST25 Discovery.
  * @details  Provides all the requires functions to abstract from the board HW.
  * @{
  */   
    
/** @defgroup ST25_DISCOVERY_LOW_LEVEL ST25 DISCOVERY LOW LEVEL
  * @brief This file provides set of firmware functions to manage Leds and push-button
  *        available on ST25 Discovery Kit from STMicroelectronics.
  * @{
  */ 


/**
  * @brief ST25 DISCOVERY BSP Driver version number V1.0.0
  * @{
  */
#define __ST25_DISCOVERY_BSP_VERSION_MAIN   (0x01) /*!< [31:24] main version */
#define __ST25_DISCOVERY_BSP_VERSION_SUB1   (0x00) /*!< [23:16] sub1 version */
#define __ST25_DISCOVERY_BSP_VERSION_SUB2   (0x00) /*!< [15:8]  sub2 version */
#define __ST25_DISCOVERY_BSP_VERSION_RC     (0x00) /*!< [7:0]  release candidate */ 
#define __ST25_DISCOVERY_BSP_VERSION        ((__ST25_DISCOVERY_BSP_VERSION_MAIN << 24)\
                                             |(__ST25_DISCOVERY_BSP_VERSION_SUB1 << 16)\
                                             |(__ST25_DISCOVERY_BSP_VERSION_SUB2 << 8 )\
                                             |(__ST25_DISCOVERY_BSP_VERSION_RC))   
/**
  * @}
  */
  

/**
  * @brief List of GPIO ports used for the LEDs.
  */
GPIO_TypeDef* GPIO_PORT[LEDn] = {LED2_GPIO_PORT, LED3_GPIO_PORT};

/**
  * @brief List of GPIO pins used for the LEDs.
  */
const uint16_t GPIO_PIN[LEDn] = {LED2_PIN,LED3_PIN};

/**
  * @brief List of GPIO ports used for the Joystick & Button.
  */
GPIO_TypeDef* BUTTON_PORT[BUTTONn] = {KEY_BUTTON_GPIO_PORT,
																			JOYSTICK_SEL_GPIO_PORT,
																			JOYSTICK_LEFT_GPIO_PORT,
																			JOYSTICK_RIGHT_GPIO_PORT,
																			JOYSTICK_UP_GPIO_PORT,
																			JOYSTICK_DOWN_GPIO_PORT}; 

/**
  * @brief List of GPIO pins used for the Joystick & Button.
  */
const uint16_t BUTTON_PIN[BUTTONn] = {KEY_BUTTON_PIN,
																			JOYSTICK_SEL_GPIO_PIN,
																			JOYSTICK_LEFT_GPIO_PIN,
																			JOYSTICK_RIGHT_GPIO_PIN,
																			JOYSTICK_UP_GPIO_PIN,
																			JOYSTICK_DOWN_GPIO_PIN}; 
 
/**
  * @brief List of interrupt lines used for the Joystick & Button.
  */
const uint8_t BUTTON_IRQn[BUTTONn] = {KEY_BUTTON_EXTI_IRQn,
																			EXTI0_IRQn,
																			EXTI0_IRQn,
																			EXTI0_IRQn,
																			EXTI0_IRQn,
																			EXTI0_IRQn};


#ifdef HAL_SPI_MODULE_ENABLED
  uint32_t SpixTimeout = SPIx_TIMEOUT_MAX; /*<! Value of Timeout when SPI communication fails */
#endif
#ifdef HAL_I2C_MODULE_ENABLED
  uint32_t I2cxTimeout = I2Cx_TIMEOUT; /*<! Value of Timeout when I2C communication fails */
#endif
#if defined(HAL_SPI_MODULE_ENABLED) && (USE_LCD)                                      
  static uint8_t Is_LCD_IO_Initialized = 0;
#endif
#if defined(HAL_ADC_MODULE_ENABLED) && defined(USE_ST25DV)
  /**
  * @brief List of Analog to Digital converter IOs.
  */
  const uint32_t ADC1_Channel_list[ADC1_CHANNELn] = ADC1_CHANNEL_LIST;
  static ADC_ChannelConfTypeDef sConfig;
#endif

/**
 * @brief BUS variables
 */

// RB: put here the peripheral BUS handles 
#ifdef HAL_SPI_MODULE_ENABLED
  static SPI_HandleTypeDef hspi2;
#endif
#ifdef HAL_ADC_MODULE_ENABLED
  static ADC_HandleTypeDef hadc1;
#endif
#ifdef HAL_I2C_MODULE_ENABLED
  static I2C_HandleTypeDef hi2c1;
  static I2C_HandleTypeDef hi2c2;
  static I2C_HandleTypeDef hi2c3;
#endif

#ifdef HAL_I2C_MODULE_ENABLED
  /* I2Cx bus function */
  static void       I2C1_Init(void);
  static void       I2C1_ITConfig(void);
  static void       I2C1_WriteData(uint8_t Addr, uint8_t Reg, uint8_t Value);
  static void       I2C1_WriteBuffer(uint8_t Addr, uint8_t Reg,  uint8_t *pBuffer, uint16_t Length);
  static uint8_t    I2C1_ReadData(uint8_t Addr, uint8_t Reg);
  static uint8_t    I2C1_ReadBuffer(uint8_t Addr, uint8_t Reg, uint8_t *pBuffer, uint16_t Length);
  static void       I2C1_Error(void);
  static void       I2C1_MspInit(I2C_HandleTypeDef *hi2c);  

  #if defined(USE_NFCTAG)
    static HAL_StatusTypeDef STM32_I2C2_Init( void );
    static HAL_StatusTypeDef STM32_I2C2_MemWrite( const uint8_t * const pData, const uint8_t DevAddr, const uint16_t TarAddr, const uint16_t Size );
    static HAL_StatusTypeDef STM32_I2C2_MemRead( uint8_t * const pData, const uint8_t DevAddr, const uint16_t TarAddr, const uint16_t Size );
    static HAL_StatusTypeDef STM32_I2C2_Read( uint8_t * const pData, const uint8_t DevAddr, const uint16_t Size );
    static uint8_t           STM32_I2C2_IsNacked( void );
    static HAL_StatusTypeDef STM32_I2C2_IsDeviceReady( const uint8_t DevAddr, const uint32_t Trials );
    static void              STM32_I2C2_MspInit( void );
    static void              STM32_I2C2_MspDeInit( void );
  #endif

  static void       I2C3_Init(void);
  static void       I2C3_WriteBuffer(uint8_t Addr, uint8_t *pBuffer, uint16_t Length);
  static uint8_t    I2C3_ReadData(uint8_t Addr);
  static void       I2C3_Error(void);
  static void       I2C3_MspInit(I2C_HandleTypeDef *hi2c);  
#endif

#if defined(HAL_ADC_MODULE_ENABLED) && defined(USE_ST25DV)
  static void       ADCx_Init(void);
  static void       ADCx_DeInit(void);
  static void       ADCx_MspInit(ADC_HandleTypeDef *hadc);
  static void 			ADCx_MspDeInit(ADC_HandleTypeDef *hadc);
#endif

#if defined(HAL_SPI_MODULE_ENABLED) && (USE_LCD)
  static void       SPI2_Init(void);
  static uint32_t   SPI2_Read(uint8_t ReadSize, uint8_t *buffer);
  static void       SPI2_Write(uint16_t Value);
  static void       SPI2_Error(void);
  static void       SPI2_MspInit(SPI_HandleTypeDef *hspi);
#endif

#if defined(HAL_SPI_MODULE_ENABLED) && defined(USE_LCD)
  /* LCD IO functions */
  void              LCD_IO_Init(void);
  void              LCD_IO_WriteData(uint8_t Data);
  void 							LCD_IO_WriteData16(uint16_t RegValue);
  void 							LCD_IO_WriteMultipleData(uint8_t *pData, uint32_t Size);
  uint32_t 					LCD_IO_ReadData(uint16_t RegValue, uint8_t ReadSize);
  void              LCD_IO_WriteReg(uint8_t LCDReg);
  void              LCD_Delay(uint32_t delay);
#endif

#ifdef HAL_I2C_MODULE_ENABLED
  /* IOExpander IO functions */
  void                      IOE_Init(void);
  void                      IOE_ITConfig(void);
  void                      IOE_Delay(uint32_t Delay);
  void                      IOE_Write(uint8_t Addr, uint8_t Reg, uint8_t Value);
  uint8_t                   IOE_Read(uint8_t Addr, uint8_t Reg);
  uint16_t                  IOE_ReadMultiple(uint8_t Addr, uint8_t Reg, uint8_t *pBuffer, uint16_t Length);
  void                      IOE_WriteMultiple(uint8_t Addr, uint8_t Reg, uint8_t *pBuffer, uint16_t Length);

/* DPOT IO functions */
void DPOT_Init(void);
uint8_t DPOT_Read(uint8_t Addr);
void DPOT_WriteBuffer(uint8_t Addr, uint8_t *pBuf, uint16_t Len);
#endif

#if defined(USE_NFCTAG)
  /* NFCTAG IO functions */
  static NFCTAG_StatusTypeDef NFCTAG_IO_Init( void );
  NFCTAG_StatusTypeDef NFCTAG_IO_MemWrite( const uint8_t * const pData, const uint8_t DevAddr, const uint16_t TarAddr, const uint16_t Size );
  NFCTAG_StatusTypeDef NFCTAG_IO_MemRead( uint8_t * const pData, const uint8_t DevAddr, const uint16_t TarAddr, const uint16_t Size );
  NFCTAG_StatusTypeDef NFCTAG_IO_Read( uint8_t * const pData, const uint8_t DevAddr, const uint16_t Size );
  uint8_t              NFCTAG_IO_IsNacked( void );
  NFCTAG_StatusTypeDef NFCTAG_IO_IsDeviceReady( const uint8_t DevAddr, const uint32_t Trials );

  NFCTAG_StatusTypeDef NFCMEM_IO_Init( void );
  NFCTAG_StatusTypeDef NFCMEM_IO_MemWrite( const uint8_t * const pData, const uint8_t DevAddr, const uint16_t TarAddr, const uint16_t Size );
  NFCTAG_StatusTypeDef NFCMEM_IO_MemRead( uint8_t * const pData, const uint8_t DevAddr, const uint16_t TarAddr, const uint16_t Size );
  NFCTAG_StatusTypeDef NFCMEM_IO_Read( uint8_t * const pData, const uint8_t DevAddr, const uint16_t Size );
  uint8_t              NFCMEM_IO_IsNacked( void );
  NFCTAG_StatusTypeDef NFCMEM_IO_IsDeviceReady( const uint8_t DevAddress, const uint32_t Trials );
#endif

/**
  * @brief  This method returns the ST25 DISCOVERY BSP Driver revision
  * @retval version: 0xXYZR (8bits for each decimal, R for RC)
  */
uint32_t BSP_GetVersion(void)
{
  return __ST25_DISCOVERY_BSP_VERSION;
}

/**
  * @brief  Configures LED GPIO.
  * @param  Led: Specifies the Led to be configured. 
  *   This parameter can be one of following parameters:
  *     @arg LED2
  */
void BSP_LED_Init(Led_TypeDef Led)
{
  GPIO_InitTypeDef  GPIO_InitStruct;
  
  /* Enable the GPIO_LED Clock */
  LEDx_GPIO_CLK_ENABLE(Led);
  
  /* Configure the GPIO_LED pin */
  GPIO_InitStruct.Pin = GPIO_PIN[Led];
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
  
  HAL_GPIO_Init(GPIO_PORT[Led], &GPIO_InitStruct);
  
  HAL_GPIO_WritePin(GPIO_PORT[Led], GPIO_PIN[Led], GPIO_PIN_RESET); 
}

/**
  * @brief  DeInit LEDs.
  * @param  Led: LED to be de-init. 
  *   This parameter can be one of the following values:
  *     @arg  LED2
  * @note Led DeInit does not disable the GPIO clock nor disable the Mfx 
  */
void BSP_LED_DeInit(Led_TypeDef Led)
{
  GPIO_InitTypeDef  gpio_init_structure;

  /* Turn off LED */
  HAL_GPIO_WritePin(GPIO_PORT[Led], GPIO_PIN[Led], GPIO_PIN_RESET);
  /* DeInit the GPIO_LED pin */
  gpio_init_structure.Pin = GPIO_PIN[Led];
  HAL_GPIO_DeInit(GPIO_PORT[Led], gpio_init_structure.Pin);
}

/**
  * @brief  Turns selected LED On.
  * @param  Led: Specifies the Led to be set on. 
  *   This parameter can be one of following parameters:
  *     @arg LED2
  */
void BSP_LED_On(Led_TypeDef Led)
{
  HAL_GPIO_WritePin(GPIO_PORT[Led], GPIO_PIN[Led], GPIO_PIN_SET); 
}

/**
  * @brief  Turns selected LED Off.
  * @param  Led: Specifies the Led to be set off. 
  *   This parameter can be one of following parameters:
  *     @arg LED2
  */
void BSP_LED_Off(Led_TypeDef Led)
{
  HAL_GPIO_WritePin(GPIO_PORT[Led], GPIO_PIN[Led], GPIO_PIN_RESET); 
}

/**
  * @brief  Toggles the selected LED.
  * @param  Led: Specifies the Led to be toggled. 
  *   This parameter can be one of following parameters:
  *     @arg LED2  
  */
void BSP_LED_Toggle(Led_TypeDef Led)
{
  HAL_GPIO_TogglePin(GPIO_PORT[Led], GPIO_PIN[Led]);
}

/**
  * @brief  Configures Button GPIO and EXTI Line.
  * @param  Button: Specifies the Button to be configured.
  *   This parameter should be: BUTTON_KEY
  * @param  ButtonMode: Specifies Button mode.
  *   This parameter can be one of following parameters:   
  *     @arg BUTTON_MODE_GPIO: Button will be used as simple IO 
  *     @arg BUTTON_MODE_EXTI: Button will be connected to EXTI line with interrupt
  *                            generation capability  
  */
void BSP_PB_Init(Button_TypeDef Button, ButtonMode_TypeDef ButtonMode)
{
  GPIO_InitTypeDef GPIO_InitStruct;
  
  /* Enable the BUTTON Clock */
  BUTTONx_GPIO_CLK_ENABLE(Button);
  
  if(ButtonMode == BUTTON_MODE_GPIO)
  {
    /* Configure Button pin as input */
    GPIO_InitStruct.Pin = BUTTON_PIN[Button];
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull = GPIO_PULLDOWN;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
    HAL_GPIO_Init(BUTTON_PORT[Button], &GPIO_InitStruct);
  }
  
  if(ButtonMode == BUTTON_MODE_EXTI)
  {
    /* Configure Button pin as input with External interrupt */
    GPIO_InitStruct.Pin = BUTTON_PIN[Button];
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING; 
    HAL_GPIO_Init(BUTTON_PORT[Button], &GPIO_InitStruct);
    
    /* Enable and set Button EXTI Interrupt to the lowest priority */
    HAL_NVIC_SetPriority((IRQn_Type)(BUTTON_IRQn[Button]), 0x0F, 0x00);
    HAL_NVIC_EnableIRQ((IRQn_Type)(BUTTON_IRQn[Button]));
  }
}

/**
  * @brief  Push Button DeInit.
  * @param  Button: Button to be configured
  *   This parameter should be: BUTTON_KEY
  * @note PB DeInit does not disable the GPIO clock
  */
void BSP_PB_DeInit(Button_TypeDef Button)
{
    GPIO_InitTypeDef gpio_init_structure;

    gpio_init_structure.Pin = BUTTON_PIN[Button];
    HAL_NVIC_DisableIRQ((IRQn_Type)(BUTTON_IRQn[Button]));
    HAL_GPIO_DeInit(BUTTON_PORT[Button], gpio_init_structure.Pin);
}

/**
  * @brief  Returns the selected Button state.
  * @param  Button: Specifies the Button to be checked.
  *   This parameter should be: BUTTON_KEY  
  * @retval The Button GPIO pin value.
  */
uint32_t BSP_PB_GetState(Button_TypeDef Button)
{
  return !HAL_GPIO_ReadPin(BUTTON_PORT[Button], BUTTON_PIN[Button]);
}

#if defined(USE_NFCTAG) && defined(USE_ST25DV)
/**
  * @brief  This function initialize the GPIO to manage the NFCTAG GPO pin
  * @param  None
  * @retval None
  */
void BSP_GPO_Init( uint32_t it_mode )
{
  GPIO_InitTypeDef GPIO_InitStruct;
  NFCMEM_GPO_CLK_ENABLE( );

  GPIO_InitStruct.Pin   = NFCMEM_GPO_PIN;
  GPIO_InitStruct.Mode  = it_mode;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  GPIO_InitStruct.Pull  = GPIO_NOPULL;
  HAL_GPIO_Init( NFCMEM_GPO_PIN_PORT, &GPIO_InitStruct );
}

/**
  * @brief  This function initialize the GPIO to manage the NFCTAG LPD pin
  * @param  None
  * @retval None
  */
void BSP_LPD_Init( void )
{
  GPIO_InitTypeDef GPIO_InitStruct;
  NFCMEM_LPD_CLK_ENABLE( );

  GPIO_InitStruct.Pin   = NFCMEM_LPD_PIN;
  GPIO_InitStruct.Mode  = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  GPIO_InitStruct.Pull  = GPIO_NOPULL;
  HAL_GPIO_Init( NFCMEM_LPD_PIN_PORT, &GPIO_InitStruct );
  
  HAL_GPIO_WritePin( NFCMEM_LPD_PIN_PORT, NFCMEM_LPD_PIN, GPIO_PIN_RESET );
}

/**
  * @brief  This function get the GPIO value through GPIO
  * @param  None
  * @retval HAL GPIO pin status
  */
GPIO_PinState BSP_LPD_ReadPin( void )
{
  return HAL_GPIO_ReadPin( NFCMEM_LPD_PIN_PORT, NFCMEM_LPD_PIN );
}

/**
  * @brief  This function get the GPIO value through GPIO
  * @param  None
  * @retval HAL GPIO pin status
  */
void BSP_LPD_WritePin( GPIO_PinState LpdPinState )
{
  HAL_GPIO_WritePin( NFCMEM_LPD_PIN_PORT, NFCMEM_LPD_PIN, LpdPinState );
}
#endif

/******************************** LINK EEPROM COMPONENT *****************************/
#if defined(USE_NFCTAG)
/**
  * @brief  Initializes peripherals used by the I2C NFCTAG driver
  * @param  None
  * @retval NFCTAG enum status
  */
NFCTAG_StatusTypeDef NFCMEM_IO_Init( void )
{
  return NFCTAG_IO_Init( );
}

__weak void NFCMEM_IO_MemWriteCompleted_Callback(uint32_t size)
{
  /* This callback can be implemented elsewhere */ 
}

/**
  * @brief  Write data, at specific address, through i2c to the ST25DV
  * @param  pData: pointer to the data to write
  * @param  DevAddr : Target device address
  * @param  TarAddr : I2C data memory address to write
  * @param  Size : Size in bytes of the value to be written
  * @retval NFCTAG enum status
  */
NFCTAG_StatusTypeDef NFCMEM_IO_MemWrite( const uint8_t * const pData, const uint8_t DevAddr, const uint16_t TarAddr, const uint16_t Size )
{
  NFCTAG_StatusTypeDef pollstatus;
  NFCTAG_StatusTypeDef ret;
  uint32_t tickstart;
  
  ret = NFCTAG_IO_MemWrite( pData, DevAddr, TarAddr, Size );
  if( ret == NFCTAG_OK )
  {
    /* Poll until EEPROM is available */
    tickstart = HAL_GetTick();
    /* Wait until ST25DV is ready or timeout occurs */
    do
    {
      pollstatus = NFCMEM_IO_IsDeviceReady( DevAddr, 1 );
    } while( ( (HAL_GetTick() - tickstart) < NFCMEM_I2C_TIMEOUT) && (pollstatus != NFCTAG_OK) );
    
    if( pollstatus != NFCTAG_OK )
    {
      ret = NFCTAG_TIMEOUT;
    }
  }
  else
  {
    /* Check if Write was NACK */
    if( NFCMEM_IO_IsNacked() == I2CANSW_NACK )
    {
      ret = NFCTAG_NACK;
    }
  }
  
  NFCMEM_IO_MemWriteCompleted_Callback(Size);
  
  return ret;
}

/**
  * @brief  Reads data at a specific address from the NFCTAG.
  * @param  pData: pointer to store read data
  * @param  DevAddr : Target device address
  * @param  TarAddr : I2C data memory address to read
  * @param  Size : Size in bytes of the value to be read
  * @retval NFCTAG enum status
  */
NFCTAG_StatusTypeDef NFCMEM_IO_MemRead( uint8_t * const pData, const uint8_t DevAddr, const uint16_t TarAddr, const uint16_t Size )
{
  return NFCTAG_IO_MemRead( pData, DevAddr, TarAddr, Size );
}

/**
  * @brief  Reads data at current address from the NFCTAG.
  * @param  pData: pointer to store read data
  * @param  DevAddr : Target device address
  * @param  Size : Size in bytes of the value to be read
  * @retval NFCTAG enum status
  */
NFCTAG_StatusTypeDef NFCMEM_IO_Read( uint8_t * const pData, const uint8_t DevAddr, const uint16_t Size )
{
  return NFCTAG_IO_Read( pData, DevAddr, Size );
}

/**
  * @brief  Checks if NACK was received from I2C Slave
  * @param  None
  * @retval 0 ACK, 1 NACK
  */
uint8_t NFCMEM_IO_IsNacked( void )
{
  return NFCTAG_IO_IsNacked( );
}

/**
* @brief  Checks if target device is ready for communication
* @note   This function is used with Memory devices
* @param  DevAddr : Target device address
* @param  Trials : Number of trials
* @retval NFCTAG enum status
*/
NFCTAG_StatusTypeDef NFCMEM_IO_IsDeviceReady( const uint8_t DevAddr, const uint32_t Trials )
{ 
  return NFCTAG_IO_IsDeviceReady( DevAddr, Trials );
}

/******************************** LINK NFCTAG *****************************/
/**
  * @brief  This functions converts HAL status to NFCTAG status
  * @param  status : HAL status to convert
  * @retval NFCTAG enum status
  */
NFCTAG_StatusTypeDef NFCTAG_ConvertStatus( const HAL_StatusTypeDef status )
{
  switch( status )
  {
    case HAL_OK:
      return NFCTAG_OK;
    case HAL_ERROR:
      return NFCTAG_ERROR;
    case HAL_BUSY:
      return NFCTAG_BUSY;
    case HAL_TIMEOUT:
      return NFCTAG_TIMEOUT;
    
    default:
      return NFCTAG_TIMEOUT;
  }
}

/**
  * @brief  Configures nfctag I2C interface
  * @param  None
  * @retval NFCTAG enum status
  */
static NFCTAG_StatusTypeDef NFCTAG_IO_Init( void )
{
#if defined(USE_ST25DV)
  /* Set IT raising by default, as we don't know the correct value yet */
  BSP_GPO_Init( GPIO_MODE_IT_RISING );
  
  BSP_LPD_Init( );
#endif 
  return NFCTAG_ConvertStatus( STM32_I2C2_Init( ) );
}

/**
  * @brief  Write at specific address nfctag memory
  * @param  pData : pointer to the data to write
  * @param  DevAddr : Target device address
  * @param  TarAddr : I2C data memory address to write
  * @param  Size : Size in bytes of the value to be written
  * @retval NFCTAG enum status
  */
NFCTAG_StatusTypeDef NFCTAG_IO_MemWrite( const uint8_t * const pData, const uint8_t DevAddr, const uint16_t TarAddr, const uint16_t Size )
{
  return NFCTAG_ConvertStatus( STM32_I2C2_MemWrite( pData, DevAddr, TarAddr, Size ) );
}

/**
  * @brief  Read at specific address on nfctag
  * @param  pData : pointer to store read data
  * @param  DevAddr : Target device address
  * @param  TarAddr : I2C data memory address to read
  * @param  Size : Size in bytes of the value to be read
  * @retval NFCTAG enum status
  */
NFCTAG_StatusTypeDef NFCTAG_IO_MemRead( uint8_t * const pData, const uint8_t DevAddr, const uint16_t TarAddr, const uint16_t Size )
{
  return NFCTAG_ConvertStatus( STM32_I2C2_MemRead( pData, DevAddr, TarAddr, Size ) );
}

/**
  * @brief  Read at current address on nfctag
  * @param  pData : pointer to store read data
  * @param  DevAddr : Target device address
  * @param  Size : Size in bytes of the value to be read
  * @retval NFCTAG enum status
  */
NFCTAG_StatusTypeDef NFCTAG_IO_Read( uint8_t * const pData, const uint8_t DevAddr, const uint16_t Size )
{
  return NFCTAG_ConvertStatus( STM32_I2C2_Read( pData, DevAddr, Size ) );
}

/**
  * @brief  Checks if NACK was received from I2C Slave
  * @param  None
  * @retval 0 ACK, 1 NACK
  */
uint8_t NFCTAG_IO_IsNacked( void )
{
  return STM32_I2C2_IsNacked( );
}

/**
  * @brief  Check nfctag availability
  * @param  DevAddr : Target device address
  * @param  Trials : Number of trials
  * @retval NFCTAG enum status
  */
NFCTAG_StatusTypeDef NFCTAG_IO_IsDeviceReady( const uint8_t DevAddr, const uint32_t Trials )
{
  return NFCTAG_ConvertStatus( STM32_I2C2_IsDeviceReady( DevAddr, Trials ) );
}
#endif
/******************************************************************************
                            BUS OPERATIONS
*******************************************************************************/

/******************************* I2C Routines *********************************/
#ifdef HAL_I2C_MODULE_ENABLED
/**
  * @brief  I2Cx MSP Initialization
  * @param  hi2c: I2C handle
  */
static void I2C1_MspInit(I2C_HandleTypeDef *hi2c)
{
  GPIO_InitTypeDef  GPIO_InitStruct;  

  if (hi2c->Instance == DISCOVERY_I2C1)
  {
    /* Configure the GPIOs ---------------------------------------------------*/ 
    /* Enable GPIO clock */
    DISCOVERY_I2C1_SDA_GPIO_CLK_ENABLE();
    DISCOVERY_I2C1_SCL_GPIO_CLK_ENABLE();
      
    /* Configure I2C Tx as alternate function  */
    GPIO_InitStruct.Pin       = DISCOVERY_I2C1_SCL_PIN;
    GPIO_InitStruct.Mode      = GPIO_MODE_AF_OD;
    GPIO_InitStruct.Pull      = GPIO_NOPULL;
    GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_HIGH;
    GPIO_InitStruct.Alternate = DISCOVERY_I2C1_SCL_SDA_AF;
    HAL_GPIO_Init(DISCOVERY_I2C1_SCL_GPIO_PORT, &GPIO_InitStruct);
      
    /* Configure I2C Rx as alternate function  */
    GPIO_InitStruct.Pin = DISCOVERY_I2C1_SDA_PIN;
    HAL_GPIO_Init(DISCOVERY_I2C1_SDA_GPIO_PORT, &GPIO_InitStruct);
    
    
    /* Configure the Discovery I2Cx peripheral -------------------------------*/ 
    /* Enable I2C3 clock */
    DISCOVERY_I2C1_CLOCK_ENABLE();
    
    /* Force the I2C Peripheral Clock Reset */  
    DISCOVERY_I2C1_FORCE_RESET();
      
    /* Release the I2C Peripheral Clock Reset */  
    DISCOVERY_I2C1_RELEASE_RESET(); 
    
    /* Enable and set Discovery I2Cx Interrupt to the highest priority */
    HAL_NVIC_SetPriority(DISCOVERY_I2C1_EV_IRQn, 0x00, 0);
    HAL_NVIC_EnableIRQ(DISCOVERY_I2C1_EV_IRQn);
    
    /* Enable and set Discovery I2Cx Interrupt to the highest priority */
    HAL_NVIC_SetPriority(DISCOVERY_I2C1_ER_IRQn, 0x00, 0);
    HAL_NVIC_EnableIRQ(DISCOVERY_I2C1_ER_IRQn);  

  }
}

/**
  * @brief  I2Cx Bus initialization.
  */
static void I2C1_Init(void)
{
  if(HAL_I2C_GetState(&hi2c1) == HAL_I2C_STATE_RESET)
  {
  GPIO_InitTypeDef  GPIO_InitStruct;  
		/* workaround to avoid blocking the bus if has occured during an i2c transfer  */
    /* Enable GPIO clock */
    DISCOVERY_I2C1_SDA_GPIO_CLK_ENABLE();
    DISCOVERY_I2C1_SCL_GPIO_CLK_ENABLE();
      
    /* Configure I2C Tx as alternate function  */
    GPIO_InitStruct.Pin       = DISCOVERY_I2C1_SCL_PIN;
    GPIO_InitStruct.Pull      = GPIO_NOPULL;
    GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_HIGH;
    GPIO_InitStruct.Mode      = GPIO_MODE_OUTPUT_PP;
    HAL_GPIO_Init(DISCOVERY_I2C1_SCL_GPIO_PORT, &GPIO_InitStruct);
      
    /* Configure I2C Rx as alternate function  */
    GPIO_InitStruct.Pin = DISCOVERY_I2C1_SDA_PIN;
    GPIO_InitStruct.Mode      = GPIO_MODE_INPUT;
    HAL_GPIO_Init(DISCOVERY_I2C1_SDA_GPIO_PORT, &GPIO_InitStruct);
		uint8_t timeout = 100;
		while ((HAL_GPIO_ReadPin(DISCOVERY_I2C1_SDA_GPIO_PORT, DISCOVERY_I2C1_SDA_PIN) == GPIO_PIN_RESET) && timeout--)
		{
			HAL_GPIO_TogglePin(DISCOVERY_I2C1_SCL_GPIO_PORT, DISCOVERY_I2C1_SCL_PIN);
			HAL_Delay(5);
		}

    hi2c1.Instance              = DISCOVERY_I2C1;
    hi2c1.Init.ClockSpeed       = BSP_I2C_SPEED;
    hi2c1.Init.DutyCycle        = I2C_DUTYCYCLE_2;
    hi2c1.Init.OwnAddress1      = 0;
    hi2c1.Init.AddressingMode   = I2C_ADDRESSINGMODE_7BIT;
    hi2c1.Init.DualAddressMode  = I2C_DUALADDRESS_DISABLE;
    hi2c1.Init.OwnAddress2      = 0;
    hi2c1.Init.GeneralCallMode  = I2C_GENERALCALL_DISABLE;
    hi2c1.Init.NoStretchMode    = I2C_NOSTRETCH_DISABLE;  

    /* Init the I2C */
    I2C1_MspInit(&hi2c1);
    HAL_I2C_Init(&hi2c1);
  }
}

/**
  * @brief  Configures Interruption pin for I2C communication.
  */
static void I2C1_ITConfig(void)
{
  GPIO_InitTypeDef  GPIO_InitStruct;
    
  /* Enable the GPIO EXTI Clock */
  STMPE811_INT_CLK_ENABLE();
  
  GPIO_InitStruct.Pin   = STMPE811_INT_PIN;
  GPIO_InitStruct.Pull  = GPIO_PULLUP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  GPIO_InitStruct.Mode  = GPIO_MODE_IT_FALLING;
  HAL_GPIO_Init(STMPE811_INT_GPIO_PORT, &GPIO_InitStruct);
}

/**
  * @brief  Writes a value in a register of the device through BUS.
  * @param  Addr: Device address on BUS Bus.  
  * @param  Reg: The target register address to write
  * @param  Value: The target register value to be written 
  */
static void I2C1_WriteData(uint8_t Addr, uint8_t Reg, uint8_t Value)
  {
  HAL_StatusTypeDef status = HAL_OK;
  
  status = HAL_I2C_Mem_Write(&hi2c1, Addr, (uint16_t)Reg, I2C_MEMADD_SIZE_8BIT, &Value, 1, I2cxTimeout); 
  
  /* Check the communication status */
  if(status != HAL_OK)
  {
    /* Re-Initialize the BUS */
    I2C1_Error();
  }        
}

/**
  * @brief  Writes a value in a register of the device through BUS.
  * @param  Addr: Device address on BUS Bus.  
  * @param  Reg: The target register address to write
  * @param  pBuffer: The target register value to be written 
  * @param  Length: buffer size to be written
  */
static void I2C1_WriteBuffer(uint8_t Addr, uint8_t Reg,  uint8_t *pBuffer, uint16_t Length)
  {
  HAL_StatusTypeDef status = HAL_OK;
  
  status = HAL_I2C_Mem_Write(&hi2c1, Addr, (uint16_t)Reg, I2C_MEMADD_SIZE_8BIT, pBuffer, Length, I2cxTimeout); 

  /* Check the communication status */
  if(status != HAL_OK)
  {
    /* Re-Initialize the BUS */
    I2C1_Error();
  }        
}

/**
  * @brief  Reads a register of the device through BUS.
  * @param  Addr: Device address on BUS Bus.  
  * @param  Reg: The target register address to write
  * @retval Data read at register address
  */
static uint8_t I2C1_ReadData(uint8_t Addr, uint8_t Reg)
{
  HAL_StatusTypeDef status = HAL_OK;
  uint8_t value = 0;
  
  status = HAL_I2C_Mem_Read(&hi2c1, Addr, Reg, I2C_MEMADD_SIZE_8BIT, &value, 1, I2cxTimeout);
 
  /* Check the communication status */
  if(status != HAL_OK)
  {
    /* Re-Initialize the BUS */
    I2C1_Error();
  
  }
  return value;
}

/**
  * @brief  Reads multiple data on the BUS.
  * @param  Addr: I2C Address
  * @param  Reg: Reg Address 
  * @param  pBuffer: pointer to read data buffer
  * @param  Length: length of the data
  * @retval 0 if no problems to read multiple data
  */
static uint8_t I2C1_ReadBuffer(uint8_t Addr, uint8_t Reg, uint8_t *pBuffer, uint16_t Length)
{
  HAL_StatusTypeDef status = HAL_OK;

  status = HAL_I2C_Mem_Read(&hi2c1, Addr, (uint16_t)Reg, I2C_MEMADD_SIZE_8BIT, pBuffer, Length, I2cxTimeout);
  
  /* Check the communication status */
  if(status == HAL_OK)
  {
    return 0;
  }
  else
  {
    /* Re-Initialize the BUS */
    I2C1_Error();

    return 1;
  }
}


/**
  * @brief  I2Cx error treatment function
  */
static void I2C1_Error(void)
{
  /* De-initialize the SPI communication BUS */
  HAL_I2C_DeInit(&hi2c1);
  
  /* Re-Initialize the SPI communication BUS */
  I2C1_Init();
}

#if defined(USE_NFCTAG)
/**
  * @brief  Configures I2C interface.
  * @param  None
  * @retval HAL status
  */
static HAL_StatusTypeDef STM32_I2C2_Init( void )
{
  HAL_StatusTypeDef ret_val = HAL_OK;
  
  if( HAL_I2C_GetState(&hi2c2) == HAL_I2C_STATE_RESET )
  {
    /* I2C2 peripheral configuration */
    hi2c2.Instance = DISCOVERY_I2C2;
    hi2c2.Init.ClockSpeed = DISCOVERY_I2C2_SPEED;
    hi2c2.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
    hi2c2.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
    hi2c2.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
    hi2c2.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
    /* Init the I2C */
    STM32_I2C2_MspInit();
    ret_val = HAL_I2C_Init( &hi2c2 );
  }
  
  return ret_val;
}

void BSP_I2C2_ReInit( void )
{
 
  /* Init the I2C */
  STM32_I2C2_MspDeInit();
  HAL_I2C_DeInit( &hi2c2 );
  STM32_I2C2_Init();
  
}


/**
  * @brief  Write data in a register of the device through the bus
  * @param  pData : pointer to the data to write
  * @param  DevAddr : Target device address
  * @param  TarAddr : I2C data memory address to write
  * @param  Size : Size in bytes of the value to be written
  * @retval HAL status
  */

static HAL_StatusTypeDef STM32_I2C2_MemWrite( const uint8_t * const pData, const uint8_t DevAddr, const uint16_t TarAddr, const uint16_t Size )
{
  uint8_t *pbuffer = (uint8_t *)pData;
  
  return HAL_I2C_Mem_Write( &hi2c2, DevAddr, TarAddr, I2C_MEMADD_SIZE_16BIT, pbuffer, Size, I2Cx_TIMEOUT );
}

/**
  * @brief  Read the value of a register of the device through the bus.
  * @param  pData : pointer to store read data
  * @param  DevAddr : Target device address
  * @param  TarAddr : I2C data memory address to read
  * @param  Size : Size in bytes of the value to be read
  * @retval HAL status.
  */
static HAL_StatusTypeDef STM32_I2C2_MemRead( uint8_t * const pData, const uint8_t DevAddr, const uint16_t TarAddr, const uint16_t Size )
{
  uint8_t *pbuffer = (uint8_t *)pData;
  HAL_StatusTypeDef ret;
  /* I2C Timeout: (transfer size in bytes) * (bits per bytes) * (extra delay) / (I2C speed) */
  uint32_t timeout = (Size * 8 * 1000 * 2) / DISCOVERY_I2C2_SPEED;
  if(timeout < I2Cx_TIMEOUT)
  {
    timeout = I2Cx_TIMEOUT;
  }
  
  ret = HAL_I2C_Mem_Read( &hi2c2, DevAddr, TarAddr, I2C_MEMADD_SIZE_16BIT, pbuffer, Size, timeout);
  
  return ret;
}

/**
  * @brief  Read the value of a register of the device through the bus.
  * @param  pData : pointer to store read data
  * @param  DevAddr : the device address on bus
  * @param  Size : Size in bytes of the value to be read
  * @retval HAL status
  */
static HAL_StatusTypeDef STM32_I2C2_Read( uint8_t * const pData, const uint8_t DevAddr, const uint16_t Size )
{
  uint8_t *pbuffer = (uint8_t *)pData;
  HAL_StatusTypeDef ret;
  
  ret = HAL_I2C_Master_Receive( &hi2c2, DevAddr, pbuffer, Size, I2Cx_TIMEOUT );
  
  return ret;
}

/**
* @brief  Checks if NACK was received from I2C Slave
* @param  None
* @retval 0 ACK, 1 NACK
*/
static uint8_t STM32_I2C2_IsNacked( void )
{
  if( hi2c2.ErrorCode == HAL_I2C_ERROR_AF )
  {
    return I2CANSW_NACK;
  }
  return I2CANSW_ACK;
}

/**
* @brief  Checks if target device is ready for communication
* @param  DevAddr : Target device address
* @param  Trials : Number of trials
* @retval HAL status
*/
static HAL_StatusTypeDef STM32_I2C2_IsDeviceReady( const uint8_t DevAddr, const uint32_t Trials )
{ 
  return HAL_I2C_IsDeviceReady( &hi2c2, DevAddr, Trials, I2Cx_TIMEOUT );
}

/**
  * @brief  I2C MSP Initialization 
  *         This function configures the hardware resources used in this example: 
  *           - Peripheral's clock enable
  *           - Peripheral's GPIO Configuration  
  *           - DMA configuration for transmission request by peripheral 
  *           - NVIC configuration for DMA interrupt request enable
  * @param  None
  * @retval None
  */
static void STM32_I2C2_MspInit( void )
{  
  GPIO_InitTypeDef  GPIO_InitStruct;
  
  /* Enable I2Cx clock */
  DISCOVERY_I2C2_CLOCK_ENABLE( );

  /* Reset I2Cx */
  DISCOVERY_I2C2_FORCE_RESET( );
  DISCOVERY_I2C2_RELEASE_RESET( );
  
  /* Enable GPIO clock */
  DISCOVERY_I2C2_SDA_GPIO_CLK_ENABLE( );
  DISCOVERY_I2C2_SCL_GPIO_CLK_ENABLE( );
  
  /* I2C SCL/SDA GPIO pin configuration  */
  GPIO_InitStruct.Pin       = DISCOVERY_I2C2_SCL_PIN;
  GPIO_InitStruct.Mode      = GPIO_MODE_AF_OD;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
  GPIO_InitStruct.Alternate = DISCOVERY_I2C2_SCL_AF;
  
  HAL_GPIO_Init( DISCOVERY_I2C2_SCL_GPIO_PORT, &GPIO_InitStruct );
  
  GPIO_InitStruct.Pin       = DISCOVERY_I2C2_SDA_PIN;
  GPIO_InitStruct.Alternate = DISCOVERY_I2C2_SDA_AF;
  
  HAL_GPIO_Init( DISCOVERY_I2C2_SDA_GPIO_PORT, &GPIO_InitStruct );
}


static void STM32_I2C2_MspDeInit( void )
{  
  
  HAL_GPIO_DeInit( DISCOVERY_I2C2_SCL_GPIO_PORT, DISCOVERY_I2C2_SCL_PIN );

  HAL_GPIO_DeInit( DISCOVERY_I2C2_SDA_GPIO_PORT, DISCOVERY_I2C2_SDA_PIN );
  
    /* Disable I2Cx clock */
  __HAL_RCC_I2C2_CLK_DISABLE( );
}
#endif

/**
  * @brief  I2C3 MSP Initialization
  * @param  hi2c: I2C handle
  */
static void I2C3_MspInit(I2C_HandleTypeDef *hi2c)
{
  GPIO_InitTypeDef  GPIO_InitStruct;  

  if (hi2c->Instance == DISCOVERY_I2C3)
  {
    /* Configure the GPIOs ---------------------------------------------------*/ 
    /* Enable GPIO clock */
    DISCOVERY_I2C3_SDA_GPIO_CLK_ENABLE();
    DISCOVERY_I2C3_SCL_GPIO_CLK_ENABLE();
      
    /* Configure I2C Tx as alternate function  */
    GPIO_InitStruct.Pin       = DISCOVERY_I2C3_SCL_PIN;
    GPIO_InitStruct.Mode      = GPIO_MODE_AF_OD;
    GPIO_InitStruct.Pull      = GPIO_NOPULL;
    GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_HIGH;
    GPIO_InitStruct.Alternate = DISCOVERY_I2C3_SCL_SDA_AF;
    HAL_GPIO_Init(DISCOVERY_I2C3_SCL_GPIO_PORT, &GPIO_InitStruct);
      
    /* Configure I2C Rx as alternate function  */
    GPIO_InitStruct.Pin = DISCOVERY_I2C3_SDA_PIN;
    HAL_GPIO_Init(DISCOVERY_I2C3_SDA_GPIO_PORT, &GPIO_InitStruct);
    
    
    /* Configure the Discovery I2Cx peripheral -------------------------------*/ 
    /* Enable I2C3 clock */
    DISCOVERY_I2C3_CLOCK_ENABLE();
    
    /* Force the I2C Peripheral Clock Reset */  
    DISCOVERY_I2C3_FORCE_RESET();
      
    /* Release the I2C Peripheral Clock Reset */  
    DISCOVERY_I2C3_RELEASE_RESET(); 
    
    /* Enable and set Discovery I2Cx Interrupt to the highest priority */
    HAL_NVIC_SetPriority(DISCOVERY_I2C3_EV_IRQn, 0x00, 0);
    HAL_NVIC_EnableIRQ(DISCOVERY_I2C3_EV_IRQn);
    
    /* Enable and set Discovery I2Cx Interrupt to the highest priority */
    HAL_NVIC_SetPriority(DISCOVERY_I2C3_ER_IRQn, 0x00, 0);
    HAL_NVIC_EnableIRQ(DISCOVERY_I2C3_ER_IRQn);  

  }
}

/**
  * @brief  I2Cx Bus initialization.
  */
static void I2C3_Init(void)
{
  if(HAL_I2C_GetState(&hi2c3) == HAL_I2C_STATE_RESET)
  {
    hi2c3.Instance              = DISCOVERY_I2C3;
    hi2c3.Init.ClockSpeed       = BSP_I2C_SPEED;
    hi2c3.Init.DutyCycle        = I2C_DUTYCYCLE_2;
    hi2c3.Init.OwnAddress1      = 0;
    hi2c3.Init.AddressingMode   = I2C_ADDRESSINGMODE_7BIT;
    hi2c3.Init.DualAddressMode  = I2C_DUALADDRESS_DISABLE;
    hi2c3.Init.OwnAddress2      = 0;
    hi2c3.Init.GeneralCallMode  = I2C_GENERALCALL_DISABLE;
    hi2c3.Init.NoStretchMode    = I2C_NOSTRETCH_DISABLE;  
    
    /* Init the I2C */
    I2C3_MspInit(&hi2c3);
    HAL_I2C_Init(&hi2c3);
  }
}


/**
  * @brief  Writes a value in a register of the device through BUS.
  * @param  Addr: Device address on BUS Bus.  
  * @param  pBuffer: The target register value to be written 
  * @param  Length: buffer size to be written
  */
static void I2C3_WriteBuffer(uint8_t Addr,  uint8_t *pBuffer, uint16_t Length)
  {
  HAL_StatusTypeDef status = HAL_OK;
  
  status = HAL_I2C_Master_Transmit(&hi2c3, Addr, pBuffer, Length, I2cxTimeout); 

  /* Check the communication status */
  if(status != HAL_OK)
  {
    /* Re-Initialize the BUS */
    I2C3_Error();
  }        
}

/**
  * @brief  Reads a register of the device through BUS.
  * @param  Addr: Device address on BUS Bus.  
  * @retval Data read at register address
  */
static uint8_t I2C3_ReadData(uint8_t Addr)
{
  HAL_StatusTypeDef status = HAL_OK;
  uint8_t value = 0;
  
  status = HAL_I2C_Master_Receive(&hi2c3, Addr, &value, 1, I2cxTimeout);
 
  /* Check the communication status */
  if(status != HAL_OK)
  {
    /* Re-Initialize the BUS */
    I2C3_Error();
  
  }
  return value;
}

/**
  * @brief  I2Cx error treatment function
  */
static void I2C3_Error(void)
{
  /* De-initialize the SPI communication BUS */
  HAL_I2C_DeInit(&hi2c3);
  
  /* Re-Initialize the SPI communication BUS */
  I2C3_Init();
}
#endif

/******************************* SPI Routines *********************************/
/**
  * @brief  Initializes SPI MSP.
  */
static void SPI2_MspInit(SPI_HandleTypeDef *hspi)
{
  GPIO_InitTypeDef  GPIO_InitStruct;  
  
  /*** Configure the GPIOs ***/  
  /* Enable GPIO clock */
  DISCOVERY_SPI2_SCK_NSS_GPIO_CLK_ENABLE();
  DISCOVERY_SPI2_MISO_MOSI_GPIO_CLK_ENABLE();
  
  
  /* Configure SPI NSS */
  GPIO_InitStruct.Pin = DISCOVERY_SPI2_NSS_PIN;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull  = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  HAL_GPIO_Init(DISCOVERY_SPI2_SCK_NSS_GPIO_PORT, &GPIO_InitStruct);
  
  /* Configure SPI SCK */
  GPIO_InitStruct.Pin = DISCOVERY_SPI2_SCK_PIN;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull  = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  GPIO_InitStruct.Alternate = DISCOVERY_SPI2_AF;
  HAL_GPIO_Init(DISCOVERY_SPI2_SCK_NSS_GPIO_PORT, &GPIO_InitStruct);
  
  /* Configure SPI MISO and MOSI */ 
  GPIO_InitStruct.Pin = DISCOVERY_SPI2_MOSI_PIN;
  GPIO_InitStruct.Alternate = DISCOVERY_SPI2_AF;
  GPIO_InitStruct.Pull  = GPIO_NOPULL;
  HAL_GPIO_Init(DISCOVERY_SPI2_MISO_MOSI_GPIO_PORT, &GPIO_InitStruct);
  
  GPIO_InitStruct.Pin = DISCOVERY_SPI2_MISO_PIN;
  GPIO_InitStruct.Pull  = GPIO_NOPULL;
  HAL_GPIO_Init(DISCOVERY_SPI2_MISO_MOSI_GPIO_PORT, &GPIO_InitStruct);

  /*** Configure the SPI peripheral ***/ 
  /* Enable SPI clock */
  DISCOVERY_SPI2_CLK_ENABLE();
}

/**
  * @brief  Initializes SPI HAL.
  */
static void SPI2_Init(void)
{
  if(HAL_SPI_GetState(&hspi2) == HAL_SPI_STATE_RESET)
  {
    /* SPI Config */
    hspi2.Instance = DISCOVERY_SPI2;
   /* SPI baudrate is set to 2.6 MHz (PCLK2/SPI_BaudRatePrescaler = 84/32 = 2.6 MHz) 
       to verify these constraints:
       - ILI9341 LCD SPI interface max baudrate is 10MHz for write and 6.66MHz for read
       - PCLK2 frequency is set to 90 MHz 
    */
    hspi2.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_2;
    hspi2.Init.Direction = SPI_DIRECTION_1LINE;
    hspi2.Init.CLKPhase = SPI_PHASE_1EDGE;
    hspi2.Init.CLKPolarity = SPI_POLARITY_LOW;
    hspi2.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
    hspi2.Init.CRCPolynomial = 7;
    hspi2.Init.DataSize = SPI_DATASIZE_8BIT;
    hspi2.Init.FirstBit = SPI_FIRSTBIT_MSB;
    hspi2.Init.NSS = SPI_NSS_SOFT;
    hspi2.Init.TIMode = SPI_TIMODE_DISABLE;
    hspi2.Init.Mode = SPI_MODE_MASTER;

    SPI2_MspInit(&hspi2);
    HAL_SPI_Init(&hspi2);
  }
}


/**
  * @brief  Initializes SPI HAL for 8 bits data.
  */
static void SPI2_InitConfig_8BitsData(void)
{
     /* SPI Config */
    hspi2.Init.DataSize = SPI_DATASIZE_8BIT;
    HAL_SPI_Init(&hspi2);
}

/**
  * @brief  Initializes SPI HAL for 16 bits data.
  */
static void SPI2_InitConfig_16BitsData(void)
{
     /* SPI Config */
    hspi2.Init.DataSize = SPI_DATASIZE_16BIT;
    HAL_SPI_Init(&hspi2);
}

/**
  * @brief  Initializes SPI HAL read access (lower freq).
  */
static void SPI2_InitConfig_LowFreq(void)
{
     /* SPI Config */
  	if(hspi2.Init.BaudRatePrescaler != SPI_BAUDRATEPRESCALER_32)
    {
      hspi2.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_32;
      HAL_SPI_Init(&hspi2);
    }
}

/**
  * @brief  Initializes SPI HAL write access (higher freq).
  */
static void SPI2_InitConfig_HighFreq(void)
{
     /* SPI Config */
  	if(hspi2.Init.BaudRatePrescaler != SPI_BAUDRATEPRESCALER_2)
    {
      hspi2.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_2;
      HAL_SPI_Init(&hspi2);
    }
}

/**
  * @brief  Reads 4 bytes from device.
  * @param  ReadSize: Number of bytes to read (max 4 bytes)
  * @retval Value read on the SPI
  */
static uint32_t SPI2_Read(uint8_t ReadSize, uint8_t *buffer)
{
  HAL_StatusTypeDef status = HAL_OK;
  

  status = HAL_SPI_Receive(&hspi2, (uint8_t*) buffer, ReadSize, SpixTimeout);

 
  /* Check the communication status */
  if(status != HAL_OK)
  {
    /* Re-Initialize the BUS */
    SPI2_Error();
  }
  
  return status;
}


/**
  * @brief  SPI Write a byte to device.
  * @param  Value: value to be written
  */
static void SPI2_Write(uint16_t Value)
{
  HAL_StatusTypeDef status = HAL_OK;


  status = HAL_SPI_Transmit(&hspi2, (uint8_t*) &Value, 1, SpixTimeout);

    
  /* Check the communication status */
  if(status != HAL_OK)
  {
    /* Execute user timeout callback */
    SPI2_Error();
  }
}

/**
  * @brief  SPI error treatment function.
  */
static void SPI2_Error (void)
{
  /* De-initialize the SPI communication BUS */
  HAL_SPI_DeInit(&hspi2);
  
  /* Re-Initiaize the SPI communication BUS */
  SPI2_Init();
}

/******************************************************************************
                            LINK OPERATIONS
*******************************************************************************/
/********************************* LINK LCD ***********************************/
#if defined(HAL_SPI_MODULE_ENABLED) && (USE_LCD)
/**
  * @brief  Initializes the LCD.
  */
/**
  * @brief  Configures the LCD_SPI interface.
  */
void LCD_IO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStructure;
  
  if(Is_LCD_IO_Initialized == 0)
  {
    Is_LCD_IO_Initialized = 1; 
    
    /* Configure NCS in Output Push-Pull mode */
    LCD_WRX_GPIO_CLK_ENABLE();
    GPIO_InitStructure.Pin     = LCD_WRX_PIN;
    GPIO_InitStructure.Mode    = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStructure.Pull    = GPIO_NOPULL;
    GPIO_InitStructure.Speed   = GPIO_SPEED_FREQ_HIGH;
    HAL_GPIO_Init(LCD_WRX_GPIO_PORT, &GPIO_InitStructure);
       
    /* Configure the LCD Control pins ----------------------------------------*/
    LCD_NCS_GPIO_CLK_ENABLE();
    
    SPI2_Init();
  }
}
/**
  * @brief  Configures the LCD_SPI interface.
  */
void LCD_IO_SetDataWidth(uint32_t w16)
{
	if(w16 != hspi2.Init.DataSize)
	{
		if(w16 == SPI_DATASIZE_16BIT)
			SPI2_InitConfig_16BitsData();
		else
			SPI2_InitConfig_8BitsData();
	}
}

/**
  * @brief  Initializes SPI HAL read access (lower freq).
  */
void BSP_LCD_LowFreq(void)
{
     /* SPI Config */
    SPI2_InitConfig_LowFreq();
}

/**
  * @brief  Initializes SPI HAL write access (higher freq).
  */
void BSP_LCD_HighFreq(void)
{
  SPI2_InitConfig_HighFreq();
}

/**
  * @brief  Writes 8 bits register address.
  */
void LCD_IO_WriteReg(uint8_t Reg) 
{
  LCD_IO_SetDataWidth(SPI_DATASIZE_8BIT);
  
	/* Reset WRX to send command */
  LCD_WRX_LOW();
  
  /* Reset LCD control line(/CS) and Send command */
  LCD_CS_LOW();
  SPI2_Write(Reg);
  
  /* Deselect: Chip Select high */
  LCD_CS_HIGH();
}

/**
  * @brief  Writes writes 8 bits data value.
  */
void LCD_IO_WriteData(uint8_t RegValue) 
{
  LCD_IO_SetDataWidth(SPI_DATASIZE_8BIT);

  /* Set WRX to send data */
  LCD_WRX_HIGH();
  
  /* Reset LCD control line(/CS) and Send data */  
  LCD_CS_LOW();
  SPI2_Write(RegValue);
  
  /* Deselect: Chip Select high */
  LCD_CS_HIGH();
}

/**
  * @brief  Writes 16bits data value.
  */
void LCD_IO_WriteData16(uint16_t RegValue) 
{
  LCD_IO_SetDataWidth(SPI_DATASIZE_16BIT);
	
  /* Set WRX to send data */
  LCD_WRX_HIGH();
  
  /* Reset LCD control line(/CS) and Send data */  
  LCD_CS_LOW();
  SPI2_Write(RegValue);
  
  /* Deselect: Chip Select high */
  LCD_CS_HIGH();
}


/**
  * @brief  Reads register value.
  * @param  RegValue Address of the register to read
  * @param  ReadSize Number of bytes to read (Max = 4)
  * @retval Content of the register value
  */
uint32_t LCD_IO_ReadData(uint16_t RegValue, uint8_t ReadSize) 
{
  uint32_t readvalue = 0;

  if(ReadSize > sizeof(readvalue))
  {
    ReadSize = 4;
  }
  
  LCD_IO_SetDataWidth(SPI_DATASIZE_8BIT);
  
  /* Reset WRX to send command */
  LCD_WRX_LOW();
  
  /* Select: Chip Select low */
  LCD_CS_LOW();

  SPI2_Write(RegValue);
  SPI2_Read(ReadSize, (uint8_t *)&readvalue);

  /* Deselect: Chip Select high */
  LCD_CS_HIGH();

/* Set WRX to send data */
  LCD_WRX_HIGH();

  /* Changing endianess */
  readvalue = ((readvalue & 0xff)       << 24) | 
              ((readvalue & 0xff00)     << 8)  |
              ((readvalue & 0xff0000)   >> 8)  |
              ((readvalue & 0xff000000) >> 24);
  
  return readvalue;
}

/**
  * @brief  Wait for loop in ms.
  * @param  Delay in ms.
  */
void LCD_Delay(uint32_t Delay)
{
  HAL_Delay(Delay);
}
#endif

/*******************************************************************************
                            LINK OPERATIONS
*******************************************************************************/

/********************************* LINK IOE ***********************************/
#if defined(HAL_I2C_MODULE_ENABLED)
/**
  * @brief  IOE Low Level Initialization.
  */
void IOE_Init(void) 
{
  I2C1_Init();
}

/**
  * @brief  IOE Low Level Interrupt configuration.
  */
void IOE_ITConfig(void)
{
  I2C1_ITConfig();
}

/**
  * @brief  IOE Writes single data operation.
  * @param  Addr: I2C Address
  * @param  Reg: Reg Address 
  * @param  Value: Data to be written
  */
void IOE_Write(uint8_t Addr, uint8_t Reg, uint8_t Value)
{
  I2C1_WriteData(Addr, Reg, Value);
}

/**
  * @brief  IOE Reads single data.
  * @param  Addr: I2C Address
  * @param  Reg: Reg Address 
  * @retval The read data
  */
uint8_t IOE_Read(uint8_t Addr, uint8_t Reg)
{
  return I2C1_ReadData(Addr, Reg);
}

/**
  * @brief  IOE Writes multiple data.
  * @param  Addr: I2C Address
  * @param  Reg: Reg Address 
  * @param  pBuffer: pointer to data buffer
  * @param  Length: length of the data
  */
void IOE_WriteMultiple(uint8_t Addr, uint8_t Reg, uint8_t *pBuffer, uint16_t Length)
{
  I2C1_WriteBuffer(Addr, Reg, pBuffer, Length);
}

/**
  * @brief  IOE Reads multiple data.
  * @param  Addr: I2C Address
  * @param  Reg: Reg Address 
  * @param  pBuffer: pointer to data buffer
  * @param  Length: length of the data
  * @retval 0 if no problems to read multiple data
  */
uint16_t IOE_ReadMultiple(uint8_t Addr, uint8_t Reg, uint8_t *pBuffer, uint16_t Length)
{
 return I2C1_ReadBuffer(Addr, Reg, pBuffer, Length);
}

/**
  * @brief  IOE Delay.
  * @param  Delay in ms
  */
void IOE_Delay(uint32_t Delay)
{
  HAL_Delay(Delay);
}

/********************************* LINK DPOT *****************************/
void DPOT_Init(void)
{
  I2C3_Init();
}

uint8_t DPOT_Read(uint8_t Addr)
{
  return I2C3_ReadData(Addr);
}

void DPOT_WriteBuffer(uint8_t Addr, uint8_t *pBuf, uint16_t Len)
{
  I2C3_WriteBuffer(Addr, pBuf, Len);
}
#endif

/******************************* ADC driver ********************************/
#if defined(HAL_ADC_MODULE_ENABLED) && defined(USE_ST25DV)
/**
  * @brief  Initializes ADC MSP.
  */
static void ADCx_MspInit(ADC_HandleTypeDef *hadc)
{
  GPIO_InitTypeDef  GPIO_InitStruct;
  
  /*** Configure the GPIOs ***/  
  /* Enable GPIO clock */
  ADC1_CHANNEL_8_GPIO_CLK_ENABLE();
	ADC1_CHANNEL_9_GPIO_CLK_ENABLE();
  ADC1_CHANNEL_15_GPIO_CLK_ENABLE();

  /* Configure the selected ADC Channel as analog input */
  GPIO_InitStruct.Pin = ADC1_CHANNEL_8_PIN ;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(ADC1_CHANNEL_8_GPIO_PORT, &GPIO_InitStruct);
  
  GPIO_InitStruct.Pin = ADC1_CHANNEL_9_PIN ;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(ADC1_CHANNEL_9_GPIO_PORT, &GPIO_InitStruct);
  
  GPIO_InitStruct.Pin = ADC1_CHANNEL_15_PIN ;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(ADC1_CHANNEL_15_GPIO_PORT, &GPIO_InitStruct);
  
  /*** Configure the ADC peripheral ***/ 
  /* Enable ADC clock */
  DISCOVERY_ADCx_CLK_ENABLE();
}

/**
  * @brief  DeInitializes ADC MSP.
  * @note ADC DeInit does not disable the GPIO clock
  */
static void ADCx_MspDeInit(ADC_HandleTypeDef *hadc)
{
  GPIO_InitTypeDef  GPIO_InitStruct;

  /*** DeInit the ADC peripheral ***/ 
  /* Disable ADC clock */
  DISCOVERY_ADCx_CLK_DISABLE();

  /* Configure the selected ADC Channel as analog input */
  GPIO_InitStruct.Pin =  ADC1_CHANNEL_8_PIN;
  HAL_GPIO_DeInit(ADC1_CHANNEL_8_GPIO_PORT, GPIO_InitStruct.Pin);

  GPIO_InitStruct.Pin =  ADC1_CHANNEL_9_PIN;
  HAL_GPIO_DeInit(ADC1_CHANNEL_9_GPIO_PORT, GPIO_InitStruct.Pin);

  GPIO_InitStruct.Pin =  ADC1_CHANNEL_15_PIN;
  HAL_GPIO_DeInit(ADC1_CHANNEL_15_GPIO_PORT, GPIO_InitStruct.Pin);

  /* Disable GPIO clock has to be done by the application*/
  /*  ADC1_CHANNEL_8_GPIO_CLK_DISABLE();
			ADC1_CHANNEL_9_GPIO_CLK_DISABLE();
			ADC1_CHANNEL_15_GPIO_CLK_DISABLE();
 */
}

/**
  * @brief  Initializes ADC HAL.
  */
static void ADCx_Init(void)
{
  if(HAL_ADC_GetState(&hadc1) == HAL_ADC_STATE_RESET)
  {
    /* ADC Config */
    hadc1.Instance                   = DISCOVERY_ADCx;
    hadc1.Init.ClockPrescaler        = ADC_CLOCK_SYNC_PCLK_DIV4; /* (must not exceed 36MHz) */
    hadc1.Init.Resolution            = ADC_RESOLUTION_12B;
    hadc1.Init.DataAlign             = ADC_DATAALIGN_RIGHT;
    hadc1.Init.ScanConvMode          = ENABLE;
    hadc1.Init.ContinuousConvMode    = DISABLE;
    hadc1.Init.DiscontinuousConvMode = DISABLE;
    hadc1.Init.ExternalTrigConvEdge  = ADC_EXTERNALTRIGCONVEDGE_NONE;
    hadc1.Init.EOCSelection          = ADC_EOC_SINGLE_CONV;
    hadc1.Init.NbrOfConversion       = ADC1_CHANNELn;
    hadc1.Init.DMAContinuousRequests = DISABLE;    
   
    ADCx_MspInit(&hadc1);
    HAL_ADC_Init(&hadc1);
  }
}

/**
  * @brief  Initializes ADC HAL.
  */
static void ADCx_DeInit(void)
{
    hadc1.Instance   = DISCOVERY_ADCx;
    
    HAL_ADC_DeInit(&hadc1);
    ADCx_MspDeInit(&hadc1);
}
#endif

/******************************* LINK ST25DV EH ********************************/
#if defined(HAL_ADC_MODULE_ENABLED) && defined(USE_ST25DV)
/**
  * @brief  Configures ADC for Energy Harvesting measurement from ST25DV  
  * @retval (0=> success, 1=> fail) 
  */
uint8_t BSP_ST25DV_EH_Init(void)
{
  uint8_t status = HAL_ERROR;
   
  ADCx_Init();
   
	   /**Configure for the selected ADC regular channels their corresponding rank in the sequencer and its sample time. 
    */
	for (int i = 0 ; i<ADC1_CHANNELn;i++)
	{
		sConfig.Channel = ADC1_Channel_list[i];
		sConfig.Rank = i+1;
		sConfig.SamplingTime = ADC_SAMPLETIME_3CYCLES;
		status |= HAL_ADC_ConfigChannel(&hadc1, &sConfig);
  }

  /* Return EH meas initialization status */
  return status;
}

/**
 * @brief   This function starts the ADC and poll for conversions, stops when the sequence of conversion is over
 * @param   values	Table to save converted values 
 * @retval  None
 */
void BSP_ST25DV_EH_Measure(uint16_t *values)
{
	int i;
	
	HAL_ADC_Start(&hadc1);
	for(i=0;i<hadc1.Init.NbrOfConversion;i++)
	{
		// HAL_ADC_PollForConversion clears the EOC flag without reading the converted values
		// thus the next conversion starts before we read the previous value!
    /* TODO: add a timeout here */
		while(!(__HAL_ADC_GET_FLAG(&hadc1, ADC_FLAG_EOC))){}
    values[i] = HAL_ADC_GetValue(&hadc1);
	  __HAL_ADC_CLEAR_FLAG(&hadc1, ADC_FLAG_STRT | ADC_FLAG_EOC);
	}
	HAL_ADC_Stop(&hadc1);

}

/**
  * @brief  Deconfigures ADC for Energy Harvesting measurement from ST25DV  
  * @retval None 
  */
void BSP_ST25DV_EH_DeInit(void)
{
   
  ADCx_DeInit();
   
}
#endif

/**
  * @}
  */

/**
  * @}
  */    

/**
  * @}
  */ 
    
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
