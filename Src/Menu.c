#include "Menu.h"
#include <stdlib.h>

extern RFModule *selectedModule;
extern Statistics stat;
extern bool updateScreenIsNeeded;

volatile uint32_t tempValueU32 = 0;
volatile uint32_t tempMaxValueU32 = 0;
volatile uint32_t tempMinValueU32 = 0;
volatile uint32_t *pointerForParameterU32 = 0;
volatile uint8_t *pointerForParameterU8 = 0;

volatile bool paramChanged[9];

void InitScreen() {
   BSP_TS_Init(240,320);    
   BSP_LCD_Init();
}

void ResetStat(Statistics *stat) {
   stat->allSend = 0;
   stat->distance = 0;
   stat->durationSend = 0;
   stat->errorCRC = 0;
   stat->newDataIsReady = true;
   stat->notConfirm = 0;
   stat->precentSuccessful = 0;
   stat->RSSI = 0;
}

void PrintSelectionMenu(char *firstElement, char *secondElement, char *thirdElement, char *fourthElement, char *fivethElement, char *sixthElement, char *seventhElement) {
   BSP_LCD_Clear(LCD_COLOR_WHITE);
   BSP_LCD_SetTextColor(LCD_COLOR_RED);
   BSP_LCD_FillRect(0, 0, 320, 30);
   BSP_LCD_SetBackColor(LCD_COLOR_RED);
   BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
   BSP_LCD_SetFont(&Font24);
   BSP_LCD_DisplayStringAt(10, 2, (uint8_t *)"Select RF module", LEFT_MODE);
   BSP_LCD_SetFont(&Font20);
   BSP_LCD_SetBackColor(LCD_COLOR_WHITE);
   BSP_LCD_SetTextColor(LCD_COLOR_BLACK);
   BSP_LCD_DisplayStringAt(5, 35, (uint8_t *) firstElement, LEFT_MODE);   
   BSP_LCD_DisplayStringAt(5, 65, (uint8_t *) secondElement, LEFT_MODE);
   BSP_LCD_DisplayStringAt(5, 95, (uint8_t *) thirdElement, LEFT_MODE);
   BSP_LCD_DisplayStringAt(5, 125, (uint8_t *) fourthElement,LEFT_MODE);
   BSP_LCD_DisplayStringAt(5, 155, (uint8_t *) fivethElement, LEFT_MODE);
   BSP_LCD_DisplayStringAt(5, 185, (uint8_t *) sixthElement, LEFT_MODE);
   BSP_LCD_DisplayStringAt(5, 215, (uint8_t *) seventhElement, LEFT_MODE);
}

void PrintSelectOperationModeMenu(RFModule *module) {
   char tmp [40];
   BSP_LCD_Clear(LCD_COLOR_WHITE);
   BSP_LCD_SetTextColor(LCD_COLOR_RED);
   BSP_LCD_FillRect(0, 0, 320, 26);
   BSP_LCD_SetBackColor(LCD_COLOR_RED);
   BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
   BSP_LCD_SetFont(&Font20);
   sprintf(tmp, "%s %s", module->nameModule, "ModeSelect");
   BSP_LCD_DisplayStringAt(1, 4, (uint8_t *)tmp, LEFT_MODE);
   BSP_LCD_SetTextColor(LCD_COLOR_LIGHTGRAY);
   BSP_LCD_FillRect(85,65,150,50);
   BSP_LCD_FillRect(85,140,150,50);
   BSP_LCD_SetFont(&Font22);
   BSP_LCD_SetTextColor(LCD_COLOR_BLACK);
   BSP_LCD_SetBackColor(LCD_COLOR_LIGHTGRAY);
   
   BSP_LCD_DisplayStringAt(110,80, (uint8_t *)"Master", LEFT_MODE);
   BSP_LCD_DisplayStringAt(120,155, (uint8_t *)"Slave", LEFT_MODE);
   BSP_LCD_SetTextColor(LCD_COLOR_BLUE);
   BSP_LCD_SetBackColor(LCD_COLOR_WHITE);
   BSP_LCD_SetFont(&Font24);
   BSP_LCD_DisplayStringAt(240, 210, (uint8_t *) "Back", LEFT_MODE);
}

void PrintConfigMenu(RFModule *module) {
   char tmp [40];
   BSP_LCD_Clear(LCD_COLOR_WHITE);
   BSP_LCD_SetTextColor(LCD_COLOR_BLUE);
   BSP_LCD_FillRect(0, 0, 320, 26);
   BSP_LCD_SetBackColor(LCD_COLOR_BLUE);
   BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
   BSP_LCD_SetFont(&Font24);
   sprintf(tmp, "%s %s", module->nameModule, "config");
   BSP_LCD_DisplayStringAt(10, 2, (uint8_t *) tmp, LEFT_MODE);
   BSP_LCD_SetFont(&Font20);
   BSP_LCD_SetBackColor(LCD_COLOR_WHITE);
   BSP_LCD_SetTextColor(paramChanged[0]? LCD_COLOR_RED: LCD_COLOR_BLACK);
   sprintf(tmp, "%s: %7i", "Frequency", module->centreFr);
   BSP_LCD_DisplayStringAt(5, 30, (uint8_t *) tmp, LEFT_MODE);
   BSP_LCD_SetTextColor(paramChanged[1]? LCD_COLOR_RED: LCD_COLOR_BLACK);
   sprintf(tmp, "%s: %3.2f", "Bandwidth ", module->bandwidth);   
   BSP_LCD_DisplayStringAt(5, 54, (uint8_t *) tmp, LEFT_MODE);
   BSP_LCD_SetTextColor(paramChanged[2]? LCD_COLOR_RED: LCD_COLOR_BLACK);
   sprintf(tmp, "%s: %7i", "Time ping", module->timePing);
   BSP_LCD_DisplayStringAt(5, 78, (uint8_t *) tmp, LEFT_MODE);
   BSP_LCD_SetTextColor(paramChanged[3]? LCD_COLOR_RED: LCD_COLOR_BLACK);
   sprintf(tmp, "%s: %3i", "Payload Length", module->payloadLength);
   BSP_LCD_DisplayStringAt(5, 102, (uint8_t *) tmp,LEFT_MODE);
   BSP_LCD_SetTextColor(paramChanged[4]? LCD_COLOR_RED: LCD_COLOR_BLACK);
   sprintf(tmp, "%s: %2i", "Spreading Factor", module->spreadingFactor);
   BSP_LCD_DisplayStringAt(5, 126, (uint8_t *) tmp, LEFT_MODE);
   BSP_LCD_SetTextColor(paramChanged[5]? LCD_COLOR_RED: LCD_COLOR_BLACK);
   sprintf(tmp, "%s: %s", "Enable CRC", module->EnCRC? "true": "false");
   BSP_LCD_DisplayStringAt(5, 150, (uint8_t *) tmp, LEFT_MODE);  
   BSP_LCD_SetTextColor(paramChanged[6]? LCD_COLOR_RED: LCD_COLOR_BLACK);   
   sprintf(tmp, "%s: %3i", "Preamble", module->progPreamble);
   BSP_LCD_DisplayStringAt(5, 174, (uint8_t *) tmp, LEFT_MODE);
   BSP_LCD_SetTextColor(paramChanged[7]? LCD_COLOR_RED: LCD_COLOR_BLACK);
   sprintf(tmp, "%s: %3i", "Tx Power", module->transmitPower);
   BSP_LCD_DisplayStringAt(5, 198, (uint8_t *) tmp, LEFT_MODE);
   BSP_LCD_SetTextColor(paramChanged[8]? LCD_COLOR_RED: LCD_COLOR_BLACK);
   sprintf(tmp, "%s: %1i", "Coding Rate", module->codingRate);
   BSP_LCD_DisplayStringAt(5, 220, (uint8_t *) tmp, LEFT_MODE);
   BSP_LCD_SetTextColor(LCD_COLOR_BLUE);
   BSP_LCD_SetFont(&Font20);
   BSP_LCD_DisplayStringAt(250, 180, (uint8_t *) "Apply", LEFT_MODE);
   BSP_LCD_DisplayStringAt(260, 220, (uint8_t *) "Back", LEFT_MODE);
}

void PrintStatistic(Statistics* stat) {
   char tmp [40];
   BSP_LCD_Clear(LCD_COLOR_WHITE);
   BSP_LCD_SetTextColor(LCD_COLOR_BLUE);
   BSP_LCD_FillRect(0, 0, 320, 26);
   BSP_LCD_SetBackColor(LCD_COLOR_BLUE);
   BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
   BSP_LCD_SetFont(&Font24);
   sprintf(tmp, "%s %s", stat->nameModule, "statistic");
   BSP_LCD_DisplayStringAt(10, 2, (uint8_t *) tmp, LEFT_MODE);
   BSP_LCD_SetFont(&Font20);
   BSP_LCD_SetBackColor(LCD_COLOR_WHITE);
   BSP_LCD_SetTextColor(LCD_COLOR_BLACK);
   sprintf(tmp, "%s: %7i", "AllSend", stat->allSend);
   BSP_LCD_DisplayStringAt(5, 32, (uint8_t *) tmp, LEFT_MODE);
   sprintf(tmp, "%s: %7i", "NotConfirm  ", stat->notConfirm);   
   BSP_LCD_DisplayStringAt(5, 58, (uint8_t *) tmp, LEFT_MODE);
   sprintf(tmp, "%s: %3i", "PrecentSuccessful", stat->precentSuccessful);
   BSP_LCD_DisplayStringAt(5, 84, (uint8_t *) tmp, LEFT_MODE);
   sprintf(tmp, "%s: %3i", "RSSI ", stat->RSSI);
   BSP_LCD_DisplayStringAt(5, 110, (uint8_t *) tmp,LEFT_MODE);
   sprintf(tmp, "%s: %7i", "DurationSend", stat->durationSend);
   BSP_LCD_DisplayStringAt(5, 136, (uint8_t *) tmp, LEFT_MODE);
   sprintf(tmp, "%s: %7i", "ErrorCrc", stat->errorCRC);
   BSP_LCD_DisplayStringAt(5, 162, (uint8_t *) tmp, LEFT_MODE);   
   sprintf(tmp, "%s: %5i", "Distance", stat->distance);
   BSP_LCD_DisplayStringAt(5, 188, (uint8_t *) tmp, LEFT_MODE);
   BSP_LCD_SetTextColor(LCD_COLOR_BLUE);
   BSP_LCD_SetFont(&Font20);
   BSP_LCD_DisplayStringAt(190, 220, (uint8_t *) "ResetStat", LEFT_MODE);
   BSP_LCD_DisplayStringAt(5, 220, (uint8_t *) "Config", LEFT_MODE);
}

void PrintNumericButton(uint16_t X, uint16_t Y, char* text) {
   BSP_LCD_SetTextColor(LCD_COLOR_LIGHTGRAY); 
   BSP_LCD_FillRect(X, Y, 50, 50);
   BSP_LCD_SetFont(&Font24);
   BSP_LCD_SetBackColor(LCD_COLOR_LIGHTGRAY);
   BSP_LCD_SetTextColor(LCD_COLOR_BLACK);
   BSP_LCD_DisplayStringAt(X+17, Y+15, (uint8_t *)text, LEFT_MODE);
}

char *numeralStr[] = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};

void PrintNumericKeypad() {
  for(char i = 0; i < 10; i++) {
     if(i >= 5)
      PrintNumericButton((i - 5)*60+15, 140, numeralStr[i]);
     else 
      PrintNumericButton(i*60+15, 80, numeralStr[i]);  
  } 
   BSP_LCD_SetTextColor(LCD_COLOR_LIGHTGRAY); 
   BSP_LCD_FillRect(170, 200, 63, 30);
   BSP_LCD_FillRect(93, 200, 63, 30);
   BSP_LCD_SetFont(&Font16);
   BSP_LCD_SetBackColor(LCD_COLOR_LIGHTGRAY);
   BSP_LCD_SetTextColor(LCD_COLOR_BLACK);
   BSP_LCD_DisplayStringAt(175, 208, (uint8_t *)"Enter", LEFT_MODE);
   BSP_LCD_DisplayStringAt(98, 208, (uint8_t *)"Clear", LEFT_MODE);
  
  
}

void PrintParamChangeScreen(void* param, char typeParam, uint32_t minValue, uint32_t maxValue, char* nameParam) {
   char tmp [40];
   
   BSP_LCD_Clear(LCD_COLOR_WHITE);
   BSP_LCD_SetTextColor(LCD_COLOR_BLUE);
   BSP_LCD_FillRect(0, 0, 320, 26);
   BSP_LCD_SetBackColor(LCD_COLOR_BLUE);
   BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
   BSP_LCD_SetFont(&Font24);
   BSP_LCD_DisplayStringAt(10, 2, (uint8_t *) nameParam, LEFT_MODE);
   
   BSP_LCD_SetBackColor(LCD_COLOR_WHITE);
   BSP_LCD_SetTextColor(LCD_COLOR_BLACK);
   
   tempMaxValueU32 = maxValue;
   tempMinValueU32 = minValue;
   if(typeParam == 32) {
      pointerForParameterU32 = (uint32_t *)param;
      pointerForParameterU8 = 0;
      tempValueU32 = *(uint32_t *)param; 
      sprintf(tmp, "%7u", tempValueU32);
      BSP_LCD_DisplayStringAt(5, 40, (uint8_t *) tmp, LEFT_MODE);
      BSP_LCD_SetFont(&Font16);
      sprintf(tmp, "max:%7u", maxValue);
      BSP_LCD_DisplayStringAt(200, 30, (uint8_t *) tmp, LEFT_MODE);
      sprintf(tmp, "min:%7u", minValue);
      BSP_LCD_DisplayStringAt(200, 50, (uint8_t *) tmp, LEFT_MODE);
   } else {
      pointerForParameterU8 = (uint8_t *)param;
      pointerForParameterU32 = 0;
      tempValueU32 = *(uint8_t *)param; 
      sprintf(tmp, "%3u", tempValueU32);
      BSP_LCD_DisplayStringAt(5, 40, (uint8_t *) tmp, LEFT_MODE);
      BSP_LCD_SetFont(&Font16);
      sprintf(tmp, "max:%3u", maxValue);
      BSP_LCD_DisplayStringAt(240, 30, (uint8_t *) tmp, LEFT_MODE);
      sprintf(tmp, "min:%3u", minValue);
      BSP_LCD_DisplayStringAt(240, 50, (uint8_t *) tmp, LEFT_MODE);
   }
   
   PrintNumericKeypad();
   
   BSP_LCD_SetTextColor(LCD_COLOR_BLUE);
   BSP_LCD_SetBackColor(LCD_COLOR_WHITE);
   BSP_LCD_SetFont(&Font20);
   BSP_LCD_DisplayStringAt(250, 220, (uint8_t *) "Apply", LEFT_MODE);
   BSP_LCD_DisplayStringAt(5, 220, (uint8_t *) "Back", LEFT_MODE);
}
char *bandwidthValue[] = {"500", "250", "125", "62.5", "41.7", "31.25", "20.8", "15.6", "10.4", "7.8"}; 

void PrintChangeBandwidthValue() {
   BSP_LCD_Clear(LCD_COLOR_WHITE);
   BSP_LCD_SetTextColor(LCD_COLOR_BLUE);
   BSP_LCD_FillRect(0, 0, 320, 26);
   BSP_LCD_SetBackColor(LCD_COLOR_BLUE);
   BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
   BSP_LCD_SetFont(&Font24);
   BSP_LCD_DisplayStringAt(10, 2, (uint8_t *) "Bandwidth", LEFT_MODE);
   BSP_LCD_SetBackColor(LCD_COLOR_WHITE);
   BSP_LCD_SetTextColor(LCD_COLOR_BLACK);
   BSP_LCD_SetFont(&Font24);
   for(char i = 0; i< 10; i++) {
      if(i >= 5)
         BSP_LCD_DisplayStringAt(150, 35 + (i - 5) * 30, (uint8_t *) bandwidthValue[i], LEFT_MODE);
      else BSP_LCD_DisplayStringAt(5, 35 + i * 30, (uint8_t *) bandwidthValue[i], LEFT_MODE);
   } 
   BSP_LCD_SetTextColor(LCD_COLOR_BLUE);
   BSP_LCD_SetBackColor(LCD_COLOR_WHITE);
   BSP_LCD_SetFont(&Font24);
   BSP_LCD_DisplayStringAt(240, 215, (uint8_t *) "Back", LEFT_MODE);
}

void PrintOnOrOffScreen(char* nameParam) {
   BSP_LCD_Clear(LCD_COLOR_WHITE);
   BSP_LCD_SetTextColor(LCD_COLOR_BLUE);
   BSP_LCD_FillRect(0, 0, 320, 26);
   BSP_LCD_SetBackColor(LCD_COLOR_BLUE);
   BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
   BSP_LCD_SetFont(&Font24);
   BSP_LCD_DisplayStringAt(10, 2, (uint8_t *) nameParam, LEFT_MODE);
   
   BSP_LCD_SetBackColor(LCD_COLOR_WHITE);
   BSP_LCD_SetTextColor(LCD_COLOR_BLACK);
   BSP_LCD_DisplayStringAt(5, 40, (uint8_t *)"true", LEFT_MODE );
   BSP_LCD_DisplayStringAt(5, 90, (uint8_t *)"false", LEFT_MODE);
   
   BSP_LCD_SetTextColor(LCD_COLOR_BLUE);
   BSP_LCD_SetBackColor(LCD_COLOR_WHITE);
   BSP_LCD_SetFont(&Font24);
   BSP_LCD_DisplayStringAt(240, 215, (uint8_t *) "Back", LEFT_MODE);
}

char tmpStr[10];
uint8_t indexStr = 0;

void ClearTmpStr() {
   indexStr = 0;
   for(char i = 0; i < sizeof(tmpStr); i++) {
      tmpStr[i] = 0;
   }
}

void ClearAllTmpValue() {
   ClearTmpStr();
   tempValueU32 = 0;
   tempMaxValueU32 = 0;
   tempMinValueU32 = 0;
   pointerForParameterU32 = 0;
   pointerForParameterU8  = 0;
}

void ClearParamCanged() {
   for(char i = 0; i < 9; i++)
      paramChanged[i] = false;
}

void PrintValueOnChangeParamScreen() {
   BSP_LCD_SetFont(&Font24);
   BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
   BSP_LCD_FillRect(5, 40, 190, 24);
   BSP_LCD_SetTextColor(LCD_COLOR_BLUE);
   BSP_LCD_DisplayStringAt(5, 40, (uint8_t *) tmpStr, LEFT_MODE);
}

uint16_t TouchOnCRCScreen(uint16_t indexMenu, TS_StateTypeDef *TsState) {
   if(TsState->Y >= 35 && TsState->Y <= 60) {
      selectedModule->EnCRC = true;
      paramChanged[indexMenu - 31] = true;
      indexMenu = 3;
      updateScreenIsNeeded = true;
   } else if(TsState->Y >= 85 && TsState->Y <= 115) {
      selectedModule->EnCRC = false;
      paramChanged[indexMenu - 31] = true;
      indexMenu = 3;
      updateScreenIsNeeded = true;
   } else if (TsState->Y >= 210 && TsState->X >= 230) {
      indexMenu = 3;
      updateScreenIsNeeded = true;
   }
   return indexMenu;
}

uint16_t TouchOnParamChangeScreen(uint16_t indexMenu, TS_StateTypeDef *TsState) {
   uint32_t tmpVal = 0;
   
   if(indexStr < sizeof(tmpStr)) {
      if(TsState->Y >= 80 && TsState->Y < 130){
         if(TsState->X >= 15 && TsState->X < 65) {
            if(indexMenu == 38 || indexStr != 0) {
               tmpStr[indexStr] = *numeralStr[0];
               indexStr++;
               PrintValueOnChangeParamScreen();
            }
         } else if(TsState->X >= 75 && TsState->X < 125) {
            tmpStr[indexStr] = *numeralStr[1];
            indexStr++;
            PrintValueOnChangeParamScreen();
         } else if(TsState->X >= 135 && TsState->X < 185) {
            tmpStr[indexStr] = *numeralStr[2];
            indexStr++;
            PrintValueOnChangeParamScreen();
         } else if(TsState->X >= 195 && TsState->X < 245) {
            tmpStr[indexStr] = *numeralStr[3];
            indexStr++;
            PrintValueOnChangeParamScreen();
         } else if(TsState->X >= 255 && TsState->X < 305) {
            tmpStr[indexStr] = *numeralStr[4];
            indexStr++;
            PrintValueOnChangeParamScreen();
         }
      } else if(TsState->Y >= 140 && TsState->Y < 190) {
         if(TsState->X >= 15 && TsState->X < 65) {
            tmpStr[indexStr] = *numeralStr[5];
            indexStr++;
            PrintValueOnChangeParamScreen();
         } else if(TsState->X >= 75 && TsState->X < 125) {
            tmpStr[indexStr] = *numeralStr[6];
            indexStr++;
            PrintValueOnChangeParamScreen();
         } else if(TsState->X >= 135 && TsState->X < 185) {
            tmpStr[indexStr] = *numeralStr[7];
            indexStr++;
            PrintValueOnChangeParamScreen();
         } else if(TsState->X >= 195 && TsState->X < 245) {
            tmpStr[indexStr] = *numeralStr[8];
            indexStr++;
            PrintValueOnChangeParamScreen();
         } else if(TsState->X >= 255 && TsState->X < 305) {
            tmpStr[indexStr] = *numeralStr[9];
            indexStr++;
            PrintValueOnChangeParamScreen();
         }
      }
   }
   if(TsState->X >= 170 && TsState->X <= 210 && TsState->Y >= 200 && indexStr !=0) { // enter
      tmpVal = atoi(tmpStr);
      char tmpS[10];
      if(tmpVal > tempMaxValueU32) {
         tmpVal = tempMaxValueU32;
      } else if(tmpVal < tempMinValueU32 ) {
         tmpVal = tempMinValueU32;
      }
      tempValueU32 = tmpVal;
      ClearTmpStr(); 
      BSP_LCD_SetFont(&Font24);
      BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
      BSP_LCD_FillRect(5, 40, 190, 24);
      BSP_LCD_SetTextColor(LCD_COLOR_BLACK);
      if(pointerForParameterU32 == 0)  sprintf(tmpS, "%3u", tempValueU32);
      else sprintf(tmpS, "%7u", tempValueU32);
      BSP_LCD_DisplayStringAt(5, 40, (uint8_t *) tmpS, LEFT_MODE);
      
   } else if(TsState->X >= 93 && TsState->X <= 160 && TsState->Y >= 200 && indexStr !=0) { 
      tmpStr[--indexStr] = 0;
      PrintValueOnChangeParamScreen();
   } else if(TsState->Y >= 210 && TsState->X <= 75) { // back
      indexMenu = 3;
      ClearAllTmpValue();
      updateScreenIsNeeded = true;
   } else if(TsState->Y >= 210 && TsState->X >= 240) { // apply
      if(indexStr != 0) {
         tmpVal = atoi(tmpStr);
         if(tmpVal > tempMaxValueU32) {
            tmpVal = tempMaxValueU32;
         } else if(tmpVal < tempMinValueU32 ) {
            tmpVal = tempMinValueU32;
         }
         tempValueU32 = tmpVal;
      }
      if(pointerForParameterU32 == 0) {
         *pointerForParameterU8 = (uint8_t)tempValueU32;
      } else {
         *pointerForParameterU32 = tempValueU32;
      }
      paramChanged[indexMenu - 31] = true;
      indexMenu = 3;
      ClearAllTmpValue();
      updateScreenIsNeeded = true;
   }  
   
   return indexMenu;   
}
uint16_t SelectModuleScreen(TS_StateTypeDef *TsState) {
   uint16_t tmpIndex = 1;
   updateScreenIsNeeded = true;
   if(TsState->Y >= 35 && TsState->Y < 65) {
     selectedModule = &modules[0];
   } else if(TsState->Y >= 65 && TsState->Y < 95) {
      selectedModule = &modules[1];
   } else if(TsState->Y >= 95 && TsState->Y < 125) {
      selectedModule = &modules[2];
   } else if(TsState->Y >= 125 && TsState->Y < 155) {
      selectedModule = &modules[3];
   } else if(TsState->Y >= 155 && TsState->Y < 185) {
      selectedModule = &modules[4];
   } else if(TsState->Y >= 185 && TsState->Y < 215) {
      selectedModule = &modules[5];
   } else if(TsState->Y >= 215) {
      selectedModule = &modules[6];
   } else { 
      tmpIndex = 0;
      updateScreenIsNeeded = false;
   }      
   return tmpIndex;
}

uint16_t SelectOperationMode(uint16_t indexMenu, TS_StateTypeDef *TsState) {
   // BSP_LCD_DisplayStringAt(240, 210, (uint8_t *) "Back", LEFT_MODE);
   if(TsState->Y >= 65 && TsState->Y <=115 && TsState->X >= 85 && TsState->X <= 235) {
      selectedModule->OperationMode = MASTER; 
      selectedModule->RFModuleInit(selectedModule);
      stat.nameModule = (uint8_t *)selectedModule->nameModule;
      indexMenu = 2;
      updateScreenIsNeeded = true;
   } else if(TsState->Y >= 140 && TsState->Y <=190 && TsState->X >= 85 && TsState->X <= 235) {
      selectedModule->OperationMode = SLAVE;
      selectedModule->RFModuleInit(selectedModule);
      stat.nameModule = (uint8_t *)selectedModule->nameModule;
      indexMenu = 2;
      updateScreenIsNeeded = true;
   } else if(TsState->Y >= 200 && TsState->X >= 230) {
      selectedModule = 0;
      indexMenu = 0;
      updateScreenIsNeeded = true;
   }
   return indexMenu;
}

uint16_t TouchOnStatScreen(uint16_t indexMenu, TS_StateTypeDef *TsState) {
   if(TsState->Y >= 210 && TsState->X <= 130) {
      indexMenu = 3;
      updateScreenIsNeeded = true;
   } else if(TsState->Y >= 210 && TsState->X >= 160) {
      ResetStat(&stat);
      updateScreenIsNeeded = false;
   }
   return indexMenu;
}

uint16_t TouchOnConfigScreen(uint16_t indexMenu, TS_StateTypeDef *TsState) { // add apply and back  
   updateScreenIsNeeded = true;
   if(TsState->Y >= 175 && TsState->Y < 205 && TsState->X >= 240) { //apply
      indexMenu = 2;
      selectedModule->RFModuleConfig(selectedModule);
      ClearParamCanged();
   } else if(TsState->Y >= 220  && TsState->X >= 240) { // back
      indexMenu = 2;
      ClearParamCanged();
   } else if(TsState->Y >= 30 && TsState->Y <54) {
      indexMenu = 31;  
   } else if(TsState->Y >= 54 && TsState->Y < 78) {
      indexMenu = 32;
   } else if(TsState->Y >= 78 && TsState->Y < 102) {
      indexMenu = 33;
   } else if(TsState->Y >= 102 && TsState->Y < 126) {
      indexMenu = 34;
   } else if(TsState->Y >= 126 && TsState->Y < 150) {
      indexMenu = 35;
   } else if(TsState->Y >= 150 && TsState->Y <174) {
      indexMenu = 36;
   } else if(TsState->Y >= 174 && TsState->Y < 198 && TsState->X < 200) {
      indexMenu = 37;
   } else if(TsState->Y >= 198 && TsState->Y < 220 && TsState->X < 200) {
      indexMenu = 38;
   } else if(TsState->Y >= 220 && TsState->X < 200) {
      indexMenu = 39;
   } else updateScreenIsNeeded = false;
   
   return indexMenu;
}

uint16_t TouchOnBandwidthValueScreen(uint16_t indexMenu, TS_StateTypeDef *TsState) {
   paramChanged[1] = true;
   updateScreenIsNeeded = true;
   indexMenu = 3;
   if(TsState->Y >= 35 && TsState->Y < 65 && TsState->X <= 100) {
      selectedModule->bandwidth = atof(bandwidthValue[0]);
   } else if(TsState->Y >= 65 && TsState->Y < 95 && TsState->X <= 100) {
      selectedModule->bandwidth = atof(bandwidthValue[1]);
   } else if(TsState->Y >= 95 && TsState->Y < 125 && TsState->X <= 100) {
      selectedModule->bandwidth = atof(bandwidthValue[2]);
   } else if(TsState->Y >= 125 && TsState->Y < 155 && TsState->X <= 100) {
      selectedModule->bandwidth = atof(bandwidthValue[3]);
   } else if(TsState->Y >= 155 && TsState->Y < 185 && TsState->X <= 100) {
      selectedModule->bandwidth = atof(bandwidthValue[4]);
   } else if(TsState->Y >= 35 && TsState->Y < 65 && TsState->X >= 140) {
      selectedModule->bandwidth = atof(bandwidthValue[5]);
   } else if(TsState->Y >= 65 && TsState->Y < 95 && TsState->X >= 140) {
      selectedModule->bandwidth = atof(bandwidthValue[6]);
   } else if(TsState->Y >= 95 && TsState->Y < 125 && TsState->X >= 140) {
      selectedModule->bandwidth = atof(bandwidthValue[7]);
   } else if(TsState->Y >= 125 && TsState->Y < 155 && TsState->X >= 140) {
      selectedModule->bandwidth = atof(bandwidthValue[8]);
   } else if(TsState->Y >= 155 && TsState->Y < 185 && TsState->X >= 140) {
      selectedModule->bandwidth = atof(bandwidthValue[9]);
   } else if(TsState->Y >= 215 && TsState->X >= 240) {
      paramChanged[1] = false;
   } else {
      indexMenu = 32;
      paramChanged[1] = false;
      updateScreenIsNeeded = false;
   }
   return indexMenu;
}

uint16_t TouchProcessing(uint16_t indexMenu, TS_StateTypeDef *TsState) {
   switch(indexMenu) {
      case 0:
         indexMenu = SelectModuleScreen(TsState);
         break;
      case 1:
         indexMenu = SelectOperationMode(indexMenu, TsState);
         break;
      case 2:
         indexMenu = TouchOnStatScreen(indexMenu, TsState);
         break;
      case 3:
         indexMenu = TouchOnConfigScreen(indexMenu, TsState);
         break;
      case 31:
      case 33:
      case 34:
      case 35:
      case 37:
      case 38:
      case 39:
         indexMenu = TouchOnParamChangeScreen(indexMenu, TsState);
         break;
      case 32:
         indexMenu = TouchOnBandwidthValueScreen(indexMenu, TsState);
         break;
      case 36:
         indexMenu = TouchOnCRCScreen(indexMenu, TsState);
         break;
   } 
   return indexMenu;     
}

void UpdateScreen(uint16_t indexMenu, RFModule *module) {
   switch(indexMenu) {
      case 0:
         PrintSelectionMenu(modules[0].nameModule, modules[1].nameModule, modules[2].nameModule, modules[3].nameModule, 
                            modules[4].nameModule, modules[5].nameModule, modules[6].nameModule);
         break;
      case 1:
         PrintSelectOperationModeMenu(module);
         break;
      case 2:
         PrintStatistic(&stat);
         break;
      case 3:
         ReadPConfigParamWithRF(module);
         PrintConfigMenu(module);
         break;
      case 31:
         PrintParamChangeScreen(&module->centreFr, 32,100000, 3000000, "Frequency");
         break;
      case 32:
         PrintChangeBandwidthValue();
         break;
      case 33:
         PrintParamChangeScreen(&module->timePing, 32, 0, INT32_MAX, "Time ping");
         break;
      case 34:
         PrintParamChangeScreen(&module->payloadLength, 8, 1, 100, "Playload Length");
         break;
      case 35:
         PrintParamChangeScreen(&module->spreadingFactor, 8, 6, 12, "Spreading Factor");
         break;
      case 36:
         PrintOnOrOffScreen("Enable CRC");
         break;
      case 37:
         PrintParamChangeScreen(&module->progPreamble,8, 1, 100, "Programmed Preamble");
         break;
      case 38:
         PrintParamChangeScreen(&module->transmitPower, 8, 0, 100, "Transmit Power");
         break;
      case 39:
         PrintParamChangeScreen(&module->codingRate, 8, 1, 4, "Coding Rate");
         break;
   }
}
