#include "main.h"
#include "clock.h"
#include "Menu.h"

/* Private defines -----------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
static void SystemClock_Config(void);
static void MX_TIM6_Init(void);
/* Public variables ---------------------------------------------------------*/
/** Number of Ticks per seconds, required by the Bluetooth HCI */
const uint32_t CLOCK_SECOND = 1000;

uint16_t indexMenu = 0;
bool updateTsStateIsNeeded = false;
bool updateScreenIsNeeded = false;

TIM_HandleTypeDef htim6;
TS_StateTypeDef TsState;

Statistics stat = {
   "",
   0,
   0,
   0,
   0,
   0,
   0,
   0,
};

RFModule *selectedModule;

int main(void)
{
  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();  
  /* Configure the system clock */
  SystemClock_Config();

  HAL_Delay(50);
  MX_TIM6_Init();
   
  InitScreen(); 
  updateScreenIsNeeded = true;    
  HAL_TIM_Base_Start(&htim6); 
  HAL_TIM_Base_Start_IT(&htim6);
  NVIC_EnableIRQ(TIM6_DAC_IRQn);  
  while(1) {
     if(updateTsStateIsNeeded) {
        updateTsStateIsNeeded = false;
        BSP_TS_GetState(&TsState);
     }
     if(TsState.TouchDetected) {
        TsState.TouchDetected = 0;
        indexMenu  = TouchProcessing(indexMenu, &TsState);
        //updateScreenIsNeeded = true;
     }
     if(updateScreenIsNeeded) {
        updateScreenIsNeeded = false;
        UpdateScreen(indexMenu, selectedModule);
     }
     if(stat.newDataIsReady && indexMenu == 2) {
        stat.newDataIsReady = false;
        updateScreenIsNeeded = true;
     }
  }
}

/** @brief System Clock Configuration.
  * @details   The system Clock is configured as follow : 
  *            System Clock source            = PLL (HSE)
  *            SYSCLK(Hz)                     = 168000000
  *            HCLK(Hz)                       = 168000000
  *            AHB Prescaler                  = 1
  *            APB1 Prescaler                 = 4
  *            APB2 Prescaler                 = 2
  *            HSE Frequency(Hz)              = 8000000
  *            PLL_M                          = 8
  *            PLL_N                          = 336
  *            PLL_P                          = 2
  *            PLL_Q                          = 7
  *            VDD(V)                         = 3.3
  *            Main regulator output voltage  = Scale1 mode
  *            Flash Latency(WS)              = 5
*/
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;

  __HAL_RCC_PWR_CLK_ENABLE();

  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI|RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = 16;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;

  RCC_OscInitStruct.PLL.PLLM = 8;
  RCC_OscInitStruct.PLL.PLLN = 336;
  RCC_OscInitStruct.PLL.PLLP = 2;
  RCC_OscInitStruct.PLL.PLLQ = 7;
  HAL_RCC_OscConfig(&RCC_OscInitStruct);

  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;
  HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5);

  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

#ifdef USE_FULL_ASSERT

/**
   * @brief Reports the name of the source file and the source line number
   * where the assert_param error has occurred.
   * @param file: pointer to the source file name
   * @param line: assert_param error line source number
   */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
    ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */

}

#endif

/**
 * @brief  This function reads & return the system tick (ms).
 * @retval tClockTime Current system tick.
 */
tClockTime Clock_Time(void)
{
  return HAL_GetTick();
}

static void MX_TIM6_Init(void)
{
   __HAL_RCC_TIM6_CLK_ENABLE();  
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  htim6.Instance = TIM6;
  htim6.Init.Prescaler = 1049;
  htim6.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim6.Init.Period = 10000;
  htim6.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim6) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim6, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
}

void Error_Handler(void) {
}
